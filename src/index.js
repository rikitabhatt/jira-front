import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
//Bootstrap and jQuery libraries
import 'bootstrap/dist/css/bootstrap.min.css';
import 'jquery/dist/jquery.min.js';
import './assets/css/style.css';
import './assets/css/responsive.css';
const rootElement = document.getElementById("root");
ReactDOM.render(<App />, rootElement);
reportWebVitals();
 