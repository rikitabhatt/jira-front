import React from 'react'

export const Sidebar = () => {
    return (
        <div>
            <a id="show-sidebar" className="btn btn-sm btn-dark" href="#">
                <i className="fas fa-bars"></i>
            </a>
            <nav id="sidebar" className="sidebar-wrapper">
                <div className="sidebar-content">
                    <div className="sidebar-brand">
                        <div id="close-sidebar">
                            <span className="ti-close"></span>
                        </div>
                    </div>
                    <div className="sidebar-header">
                        <div className="project-pic">
                            <span className="ti-flag-alt"></span>
                        </div>
                        <div className="project-info">
                            <span className="project-name">I Go Wok </span>
                        </div>
                    </div>
                    <div className="sidebar-menu">
                        <ul>
                            <li>
                               
                                    <span className="ti-back-left side-icon"></span>
                                    <span>backlog</span>
                               
                            </li>
                            <li>
                                
                                    <span className="ti-layout-accordion-list side-icon"></span>
                                    <span>Active sprints</span>

                            </li>
                            <li>
                               
                                    <span className="ti-stats-up side-icon"></span>
                                    <span>Reports</span>
                             
                            </li>
                            <li>
                                
                                    <span className="ti-view-list side-icon"></span>
                                    <span>Issues</span>
                                

                            </li>
                            

                        </ul>
                    </div>
                </div>
            </nav>    
        </div>
    )
}
