import React, { useState, useEffect } from "react";
import axios from "axios";
import { useHistory, useParams } from "react-router-dom";
import {  alertService } from '../../../_services';
const AddEdituser = () => {
  let history = useHistory();
  const { id } = useParams();
  const isAddMode = !id;
  const [user, setUser] = useState({
    user_name: "",
    user_email: ""
  });

  const { user_name, user_email, } = user;
  const onInputChange = e => {
    setUser({ ...user, [e.target.name]: e.target.value });
  };

  useEffect(() => {
    loadFromData();
  }, []);

  const onSubmit = async e => {
    e.preventDefault();
   
    if(validateForm(user) == false)
    {
      e.preventDefault();
      return false;
    }  
    let data = {};
    let msg ='';
    if(!isAddMode){ 
      data= await axios.put(`http://localhost:8000/api/user/${id}`, user);
      msg = 'Edit record successfully';
    }else {  
      data= await axios.post(`http://localhost:8000/api/user/`, user);
      msg = 'Add record successfully';
    }
    if(data.data.error)
    {
      setUser({
        ...user, 
        errors: data.data.error,
      });
      return false;
    }else{
      alertService.success(msg, { keepAfterRouteChange: true });
    }
    history.push("/user");
  };
  const loadFromData = async () => {
    if (!isAddMode) { //edit 
      const result = await axios.get(`http://localhost:8000/api/user/${id}`);
      setUser(result.data.data);
    }
  };
  const validateForm = (formData) => {
   
    let errors = {};
    let formIsValid = true;
    if (!formData.user_name) {
      formIsValid = false;
      errors['user_name'] = 'Please enter user name';
    }
    if (!formData.user_email) {
      formIsValid = false;
      errors['user_email'] = 'Please enter email id';
    }
    setUser({
      ...user, 
      errors: errors,
    });

    return formIsValid;
  };
  return (
    <div className="container-fluid">
      <div className="title-bar">
          <div className="page-title">
              <h2>User Management</h2>
          </div>
      </div>
      <div className="row">
        <div className="col-md-5">
              <form className="page-form">
                <div className="form-group">
                <label className="form-label" htmlFor="open_time">User Name<span className="astric text-orange">*</span></label>
                  <input type="text" className="form-control form-control-lg" placeholder="Enter Your Name" name="user_name" value={user_name} onChange={e => onInputChange(e)} />
                  {user.errors &&<div className='errorMsg'>{user.errors.user_name}</div>}
                </div>
                <div className="form-group">
                <label className="form-label" htmlFor="open_time">User Email<span className="astric text-orange">*</span></label>
                  <input type="text" className="form-control form-control-lg" placeholder="Enter Your Email" name="user_email" value={user_email}  onChange={e => onInputChange(e)} />
                  {user.errors &&<div className='errorMsg'>{user.errors.user_email}</div>}
                </div>
                
                <button className="btn btn-warning btn-block" onClick={e => onSubmit(e)}>{isAddMode ? 'Save' : 'Update'} </button>
              </form>
          </div>
      </div>
    </div>
  );
};

export default AddEdituser;