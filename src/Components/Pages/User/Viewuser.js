import React, { useState, useEffect } from "react";
import axios from "axios";
import { useHistory, useParams , Link } from "react-router-dom";
import {  alertService } from '../../../_services';
const Viewuser= () => {
  let history = useHistory();
  const { id } = useParams();
  const [user, setUser] = useState({
    user_name: "",
    user_email: ""
  });

  const { user_name, user_email } = user;
  const onInputChange = e => {
    setUser({ ...user, [e.target.name]: e.target.value });
  };

  useEffect(() => {
    loadFromData();
  }, []);


  const loadFromData = async () => {
    const result = await axios.get(`http://localhost:8000/api/user/${id}`);
    setUser(result.data.data);
  };
  return (
    <div className="container-fluid">
      <div className="title-bar">
          <div className="page-title">
              <h2>User Management</h2>
          </div>
      </div>
      <div className="row">
        <div className="col-md-5">
            <form className="page-form">
              <div className="form-group">
              <label className="form-label">User Name<span className="astric text-orange">*</span></label>
                <input type="text" className="form-control form-control-lg"  name="user_name" value={user_name} readOnly />
              </div>
              <div className="form-group">
              <label className="form-label">User Email<span className="astric text-orange">*</span></label>
                <input type="text" className="form-control form-control-lg"  name="user_email" value={user_email} readOnly />
              </div>
              <Link className="btn btn-warning btn-block" to={"/company"}>Back </Link>
            </form>
          </div>
      </div>
    </div>
  );
};

export default Viewuser;