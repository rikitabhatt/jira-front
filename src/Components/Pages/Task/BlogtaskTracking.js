import React, { Component } from 'react'
const $ = require('jquery');
$.DataTable = require('datatables.net');
export class BlogtaskTracking extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }
    render (){
     return (
        <div className="backlog-task-details">
            <div className="task-title backlog-task-title m-b-30">
                <div className="project-pic">
                    <span className="ti-flag-alt"></span>
                </div>
                <div className="task-title-content">
                    <div className="task-project-title">
                        <a href="#" className="text-capitalize">cosnumer sketch</a>
                        <a href="#" className="text-uppercase">cs</a>
                    </div>
                    <div className="editable-task-title">
                        <form className="editable-task-form">
                            <input type="text" className="form-control-h2" id="editable-title" aria-describedby="emailHelp"
                                value="Front &amp; Design for Project Managenment"/>
                            <span className="ti-pencil title-edit-icon"></span>
                        </form>
                    </div>
                </div>
            </div>
            <div className="tracker-block">
                <p className="collapse-para m-b-5">
                    <a className="collapse-link" data-toggle="collapse" href="#collapse-backlog-details" role="button"
                        aria-expanded="false" aria-controls="collapseExample">
                        Details<span className="ti-angle-double-right"></span>
                    </a>
                </p>
                <div className="collapse show" id="collapse-backlog-details">
                    <div className="collapse-wrapper">

                        <div className="collapse-block">
                            <div className="collpase-lable">
                                Status :
                            </div>
                            <div className="collpase-content">
                                <p className="form-control-p"><span className="status-tag text-light-green border-light-green">In
                                        Progress</span></p>
                            </div>
                        </div>
                        <div className="collapse-block">
                            <div className="collpase-lable">
                                Priority :
                            </div>
                            <div className="collpase-content">
                                <p className="form-control-p">Medium</p>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div className="tracker-block">
                <p className="collapse-para m-b-5">
                    <a className="collapse-link" data-toggle="collapse" href="#collapse-people" role="button" aria-expanded="false"
                        aria-controls="collapseExample">
                        People<span className="ti-angle-double-right"></span>
                    </a>
                </p>
                <div className="collapse show" id="collapse-people">
                    <div className="collapse-wrapper">
                        <div className="collapse-block">
                            <div className="collpase-lable">
                                Assignee :
                            </div>
                            <div className="collpase-content">
                                <form className="editable-task-form">
                                    <input type="text" list="people" className="form-control-p" placeholder="Hemal Bhavsar"
                                        autocomplete="off" /><span className="ti-pencil title-edit-icon"></span>
                                    <datalist id="people">
                                        <option>Ajay patel</option>
                                        <option>Hemal Bhavsar</option>
                                        <option>Sohel khan</option>
                                        <option>Ravi Soni</option>
                                        <option>Saniy Vohra</option>
                                    </datalist>
                                </form>
                            </div>
                        </div>
                        <div className="collapse-block">
                            <div className="collpase-lable">
                                Reporter :
                            </div>
                            <div className="collpase-content">
                                <p className="form-control-p">Ashish Thakkar</p>
                            </div>
                        </div>
                        <div className="collapse-block">
                            <div className="collpase-lable">
                                Watchers :
                            </div>
                            <div className="collpase-content">
                                <p className="form-control-p">Ashish Thakkar</p>
                                <p className="form-control-p">Hemal Bhavsar</p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div className="tracker-block">
                <p className="collapse-para m-b-5">
                    <a className="collapse-link" data-toggle="collapse" href="#collapse-date" role="button" aria-expanded="false"
                        aria-controls="collapseExample">
                        Date<span className="ti-angle-double-right"></span>
                    </a>
                </p>
                <div className="collapse show" id="collapse-date">
                    <div className="collapse-wrapper">
                        <div className="collapse-block">
                            <div className="collpase-lable">
                                Created :
                            </div>
                            <div className="collpase-content">
                                <p className="form-control-p">yesterday</p>
                            </div>
                        </div>
                        <div className="collapse-block">
                            <div className="collpase-lable">
                                Updated :
                            </div>
                            <div className="collpase-content">
                                <p className="form-control-p">yesterday</p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div className="tracker-block">
                <p className="collapse-para m-b-5">
                    <a className="collapse-link" data-toggle="collapse" href="#collapse-subtask" role="button" aria-expanded="false"
                        aria-controls="collapseExample">
                        Subtask<span className="ti-angle-double-right"></span>
                    </a>
                </p>
                <div className="collapse show" id="collapse-subtask">
                    <div className="collapse-wrapper">
                        <div className="collapse-block d-block">
                            <p className="no-subtask">There are no sub-tasks</p>
                           
                            <p className="create-sub-task-p">
                                <a className="nav-link btn btn-sky-blue d-inline-block" data-toggle="modal"
                                    data-target="#create-issue-modal">Create Subtask</a></p>
                                
                        </div>


                    </div>
                </div>
            </div>

            <div className="tracker-block">
                <p className="collapse-para m-b-5">
                    <a className="collapse-link" data-toggle="collapse" href="#collapse-comments" role="button"
                        aria-expanded="false" aria-controls="collapseExample">
                        Comments<span className="ti-angle-double-right"></span>
                    </a>
                </p>
                <div className="collapse show" id="collapse-comments">
                    <div className="collapse-wrapper">
                        <div className="collapse-block d-block">
                        <div className="comments-wrapper m-t-10">
                            <div className="comments-tab-block m-b-30">
                                <p className="work-log-p m-b-5">
                                    <a href="#">
                                        <img src="images/person.jpg" alt="person image" className="person-image" /> 
                                        <span  className="work-log-person text-capitalize">Saniya vohra</span>
                                    </a> 
                                    Commented- <span className="logged-date-time">17/May/21 12:29 PM </span>
                                </p>
                                <div className="activity-content bg-skyblue">
                                    <div className="user-content">
                                        <p>1. Api Name:- user/profile</p>
                                        <p>issue:- profile_pic key missing in user_details
                                            object</p>
                                        <p>2. Api name:- course/edit</p>
                                        <p>issue:- remove intro id validation</p>
                                        <p>3. Api name:- user/dashboard</p>
                                        <p>issue:- duration is missing in last uploaded video
                                            object</p>
                                        <p>4. Api name:-</p> <a className="content-more" href="/browse/KM-180">Read more</a>
                                    </div>
                                </div>
                            </div>
                           
                        </div>
                        <p className="create-sub-task-p"></p>
                    </div>
                </div>
                </div>
            </div>
            
            <div className="tracker-block">
            <p className="collapse-para m-b-5">
                <a className="collapse-link" data-toggle="collapse" href="#collapse-time" role="button" aria-expanded="false"
                    aria-controls="collapseExample">
                    Time Tracking<span className="ti-angle-double-right"></span>
                </a>
            </p>
            <div className="collapse show" id="collapse-time">
                <div className="collapse-wrapper">
                <div className="collapse-block align-center">
                    <div className="collpase-lable">
                        Estimated :
                    </div>
                    <div className="collpase-content progress-content">
                        <div className="progress">
                            <div className="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0"
                                aria-valuemax="100"></div>
                        </div>
                        <span className="progress-content">not specified</span>
                    </div>
                </div>
                <div className="collapse-block align-center">
                    <div className="collpase-lable">
                        Remaining :
                    </div>
                    <div className="collpase-content progress-content">
                        <div className="progress">
                            <div className="progress-bar" role="progressbar"  aria-valuenow="25"
                                aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                        <span className="progress-content">30m</span>
                    </div>
                </div>
                <div className="collapse-block align-center">
                    <div className="collpase-lable">
                        Logged :
                    </div>
                    <div className="collpase-content progress-content">
                        <div className="progress">
                            <div className="progress-bar" role="progressbar"  aria-valuenow="75"
                                aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                        <span className="progress-content">30m</span>
                    </div>
                </div>

            </div>
    </div>
</div>
        </div>
      
      );
     }
}