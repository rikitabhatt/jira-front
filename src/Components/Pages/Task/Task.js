import React, { Component } from 'react'
import ReactDOM from 'react-dom';
import axios from "axios";
import {Link} from "react-router-dom";
import jqueryMin from 'jquery/dist/jquery.min';
import {  alertService } from '../../../_services';
import { BlogTask } from './Blogtask';
import { BlogtaskTracking } from './BlogtaskTracking';
//process.env.REACT_APP_WEBSITE_URL

export class Task extends Component {
    constructor(props) {
        super(props);
        this.state = {
            filterItems: [],
            activeLi : '1',
            results: [],
            loading: true,
        };

        this.state.filterItems = [
            { dataid: 1, clicked: false, title: "All" },
            { dataid: 2, clicked: false, title: "Only my issue" },
            { dataid: 3, clicked: false, title: "recently updated" },
            { dataid: 4, clicked: false, title: "open items" },
            { dataid: 5, clicked: false, title: "pending QA" }
        ];
    }
    componentDidMount() {
        // this.getData().then(() => this.sync());
        this.sync(this.state.activeLi);
    }
    componentWillUnmount(){
        this.setState({
            results :[],
          
          })
    }
    async sync(activeLi_id){
        const res =  await axios.post(`http://localhost:8000/api/tasks/${activeLi_id}`);
        this.setState({ loading: false, results: res.data.data });
    }
    handleCheck(e) {
       // alert(e.target);
       let value ;
       console.log(e.currentTarget.dataset.id);
       //<li onClick={this.handleCheck.bind(this)} data-id="1"className="filter-label">QUICK FILTERS</li>
       const { filterItems } = this.state;
       const updatedItems = filterItems.map(row => {
        if (row.dataid == e.currentTarget.dataset.id) {
            value = e.currentTarget.dataset.id;
            this.sync(e.currentTarget.dataset.id);
          return {
            ...row,
            clicked: true
          };
          
        } else {
          return {
            ...row,
            clicked: false
          };
        }
      });
      this.setState({
        activeLi :value,
        filterItems: updatedItems
      });
    }
    render (){
      
        return (
            
            <div className="container-fluid">
            <div className="title-bar">
                <div className="page-title">
                    <h2>Open issues</h2>
                    <div className="filters-wrapper">
                        <ul className="filters-list">
                        <li class="filter-label">QUICK FILTERS :</li>
                        {this.state.filterItems.map((row, index) => (
                             <li onClick={this.handleCheck.bind(this)} data-id={row.dataid} className="filter-label"><a href="javascript:void(0);" className={row.clicked ? "active" : ""}>{row.title}</a></li>          
                        ))}                        
                        </ul>
                    </div>
                </div>
            </div>
            <div className="row">
                <div className="col-md-8">
                    <div className="backlogs-wrapper">
                        <BlogTask results={this.state.results}></BlogTask>
                    </div>
                </div>
                <div className="col-md-4">
                    <BlogtaskTracking></BlogtaskTracking>
                </div>
            </div>
        </div>
        );
     }
}
