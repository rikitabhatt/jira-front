
import React, { Component } from 'react'
import ReactDOM from 'react-dom';
import axios from "axios";
import {Link} from "react-router-dom";
import jqueryMin from 'jquery/dist/jquery.min';

export class Worklog extends Component {
render (){
 return (
<div className="tab-pane fade" id="pills-worklog" role="tabpanel" aria-labelledby="pills-worklog-tab">
    <div className="work-log-wrapper tab-content-wrapper">
        <div className="work-log-block m-b-30">
            <div className="work-log-person">
                <p className="work-log-p m-b-5">
                    <a href="#">
                        <img src="images/person.jpg" alt="person image" className="person-image"/> <span className="work-log-person text-capitalize">Saniya
                            vohra</span></a> logged work - <span className="logged-date-time">17/May/21 12:29 PM
                    </span>
                </p>
                <div className="collapse-block work-log-flex align-center">
                    <div className="collpase-lable">
                        Time Spent :
                    </div>
                    <div className="collpase-content log-content">
                        <p>1 hour</p>
                        <p>For review</p>
                    </div>
                </div>
            </div>
            <div className="workl-log-acitvity">
            </div>
        </div>
        <div className="work-log-block m-b-30">

            <p className="work-log-p m-b-5">
                <a href="#">
                    <img src="images/person.jpg" alt="person image" className="person-image"/> <span className="work-log-person text-capitalize">Saniya
                        vohra</span></a> logged work - <span className="logged-date-time">17/May/21 12:29 PM
                </span>
            </p>
            <div className="collapse-block work-log-flex align-center">
                <div className="collpase-lable">
                    Time Spent :
                </div>
                <div className="collpase-content log-content">
                    <p>1 hour</p>
                    <p>For review</p>
                </div>
            </div>


        </div>
        <div className="work-log-block m-b-30">
            <p className="work-log-p m-b-5">
                <a href="#">
                    <img src="images/person.jpg" alt="person image" className="person-image"/> <span className="work-log-person text-capitalize">Saniya
                        vohra</span></a> logged work - <span className="logged-date-time">17/May/21 12:29 PM
                </span>
            </p>
            <div className="collapse-block work-log-flex align-center">
                <div className="collpase-lable">
                    Time Spent :
                </div>
                <div className="collpase-content log-content">
                    <p>1 hour</p>
                    <p>For review</p>
                </div>
            </div>
        </div>
    </div>
</div>
 );  
}
}