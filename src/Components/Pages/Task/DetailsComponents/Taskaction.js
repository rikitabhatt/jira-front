
import React, { Component } from 'react'
import ReactDOM from 'react-dom';
import axios from "axios";
import {Link} from "react-router-dom";
import jqueryMin from 'jquery/dist/jquery.min';
import CreateTaskModal from '../../../Modal/CreateTaskModal';
import UpdateTaskAssigneeModal from '../../../Modal/UpdateTaskAssigneeModal';
import { alertService } from '../../../../_services';
export class Taskaction extends Component {
    constructor(props) {
        super(props);
         this.state = {
            results:[],
            loading: true,
        };
        
    }
    
    async updateStatusAsOpenQuestion(){
        
        const res = await axios.post('http://localhost:8000/api/task/updateTaskStatus', {
            task_id:this.props.TaskDetails.id,
            old_status:this.props.TaskDetails.status,
            new_status: 'open_question',
            created_by: 1
        });
        alertService.success('Update task successfully', { keepAfterRouteChange: true });
        //this.sync(this.TaskDetails.id);
    }
    async updateStatus(){
        let current_status = this.props.TaskDetails.status;
        let new_status = '';
        const res = await axios.post('http://localhost:8000/api/task/updateTaskStatus', {
            task_id:this.props.TaskDetails.id,
            old_status:this.props.TaskDetails.status,
            new_status: 'open_question',
            created_by: 1
        });
        alertService.success('Update task successfully', { keepAfterRouteChange: true });
        //this.sync(this.TaskDetails.id);
    }
render (){ 
    //console.log(this.props.TaskDetails); 
    let popupType = 'edit';
 return (
   
    <div className="task-actions-wrapper">
        <ul className="task-action-buttons">
            
            <CreateTaskModal  Component={this.sync} TaskId={this.props.TaskDetails.id} popupType={popupType}></CreateTaskModal>
            <li>
                <a href="#pills-comments" className="btn btn-sky-blue task-action-btn btn-icon"><span className="ti-comment"></span>Comment</a>
            </li>
           <UpdateTaskAssigneeModal Component={this.sync} AssigneeId={this.props.TaskDetails.assignee_id} TaskId={this.props.TaskDetails.id} ProjectId={this.props.TaskDetails.project_id}></UpdateTaskAssigneeModal>

           
            <li className="dropdown actions-more-dropdown">
                <a className="btn btn-sky-blue task-action-btn btn-icon dropdown-toggle" href="#" id="moreDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    More
                </a>
                <div className="dropdown-menu" aria-labelledby="moreDropdown">
                    <a className="dropdown-item" href="#">Log work </a>
                    <a className="dropdown-item" href="#">Create Sub task</a>

                </div>
            </li>
            <li>
                <a onClick={()=>{this.updateStatusAsOpenQuestion()}} className="btn btn-sky-blue task-action-btn btn-icon"><span className="ti-help-alt"></span>Open
                    question</a>
            </li>
            <li>
                <a onClick={()=>{this.updateStatus()}} className="btn btn-sky-blue task-action-btn btn-icon"><span className="ti-bar-chart"></span>TODO</a>
            </li>
        </ul>
    </div>
);
}
}
