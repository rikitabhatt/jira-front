import React, { Component } from 'react'
import ReactDOM from 'react-dom';
import axios from "axios";
import {Link} from "react-router-dom";
import jqueryMin from 'jquery/dist/jquery.min';

export class Subtask extends Component {
render (){
 return (
    <div className="tracker-block  border-yellow border-left-2 p-l-10">
        <p className="collapse-para m-b-5">
            <a className="collapse-link" data-toggle="collapse" href="#collapse-subtask" role="button" aria-expanded="false" aria-controls="collapseExample">
                Subtask<span className="ti-angle-double-right"></span>
            </a>
        </p>
        <div className="collapse show" id="collapse-subtask">
            <div className="collapse-wrapper">
                <div className="collapse-block d-block">
                    <p className="no-subtask">There are no sub-tasks</p>
                    
                    <p className="create-sub-task-p">
                        <a className="nav-link btn btn-sky-blue d-inline-block" data-toggle="modal" data-target="#create-issue-modal">Create
                            Subtask</a>
                        
                </p></div>
            </div>
        </div>
    </div>
    );
  }
}

