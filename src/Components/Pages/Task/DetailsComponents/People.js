import React, { Component } from 'react'
import ReactDOM from 'react-dom';
import axios from "axios";
import {Link} from "react-router-dom";
import jqueryMin from 'jquery/dist/jquery.min';
import { getAllAsssigneeProject} from '../../../Helper/Helper'; 
import {  alertService } from '../../../../_services';

export class People extends Component {
    constructor(props) {
        super(props);
         this.state = {
            assigneeDp :[],
            loading: true,
        };
        this.TaskDetails = props.TaskDetails;
    }
    componentDidMount() {
        this.sync();
        console.log('Data :='+this.TaskDetails.id);
    }
    async sync(){ 
        const Userdp = getAllAsssigneeProject(this.TaskDetails.project_id);  
        this.setState({ loading: false, assigneeDp: (await Userdp).data.data });
        
    }
render (){
    let TaskDetails= this.props.TaskDetails;
 return (
    <div className="tracker-block border-light-green border-left-2 p-l-10">
        <p className="collapse-para m-b-5">
            <a className="collapse-link" data-toggle="collapse" href="#collapse-people" role="button" aria-expanded="false" aria-controls="collapseExample">
                People<span className="ti-angle-double-right"></span>
            </a>
        </p>
        <div className="collapse show" id="collapse-people">
            <div className="collapse-wrapper">
                <div className="collapse-block">
                    <div className="collpase-lable">
                        Assignee :
                    </div>
                    <div className="collpase-content">
                        <form className="editable-task-form">
                            <input type="text" list="people" className="form-control-p"  autoComplete="off"/><span className="ti-pencil title-edit-icon"></span>
                            <select id="people">
                                {this.state.assigneeDp.map((row, index) => (
                                    <option value={row.user_id}>{row.user_name}</option>
                                ))}
                              
                            </select>
                        </form>
                    </div>
                </div>
                <div className="collapse-block">
                    <div className="collpase-lable">
                        Reporter :
                    </div>
                    <div className="collpase-content">
                        <p className="form-control-p">Ashish Thakkar</p>
                    </div>
                </div>
                <div className="collapse-block">
                    <div className="collpase-lable">
                        Watchers :
                    </div>
                    <div className="collpase-content">
                        <p className="form-control-p">Ashish Thakkar</p>
                        <p className="form-control-p">Hemal Bhavsar</p>
                    </div>
                </div>

            </div>
        </div>
    </div>
    );
  }
}
