
import React, { Component } from 'react'
import ReactDOM from 'react-dom';
import axios from "axios";
import {Link} from "react-router-dom";
import jqueryMin from 'jquery/dist/jquery.min';

export class Timetracking extends Component {
render (){
 return (
    <div className="tracker-block  border-blue border-left-2 p-l-10">
        <p className="collapse-para m-b-5">
            <a className="collapse-link" data-toggle="collapse" href="#collapse-time" role="button" aria-expanded="false" aria-controls="collapseExample">
                Time Tracking<span className="ti-angle-double-right"></span>
            </a>
        </p>
        <div className="collapse show" id="collapse-time">
            <div className="collapse-wrapper">
                <div className="collapse-block align-center">
                    <div className="collpase-lable">
                        Estimated :
                    </div>
                    <div className="collpase-content progress-content">
                        <div className="progress">
                            <div className="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                        <span className="progress-content">not specified</span>
                    </div>
                </div>
                <div className="collapse-block align-center">
                    <div className="collpase-lable">
                        Remaining :
                    </div>
                    <div className="collpase-content progress-content">
                        <div className="progress">
                            <div className="progress-bar" role="progressbar"  aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                        <span className="progress-content">30m</span>
                    </div>
                </div>
                <div className="collapse-block align-center">
                    <div className="collpase-lable">
                        Logged :
                    </div>
                    <div className="collpase-content progress-content">
                        <div className="progress">
                            <div className="progress-bar" role="progressbar"  aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                        <span className="progress-content">30m</span>
                    </div>
                </div>

            </div>
        </div>
    </div>

    );
  }
}

