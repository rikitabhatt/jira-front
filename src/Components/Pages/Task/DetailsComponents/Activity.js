import React, { Component } from 'react'
import ReactDOM from 'react-dom';
import axios from "axios";
import {Link} from "react-router-dom";
import jqueryMin from 'jquery/dist/jquery.min';

export class Activity extends Component {
render (){
 return (
    <div className="tab-pane fade" id="pills-activity" role="tabpanel" aria-labelledby="pills-activity-tab">
        <div className="activity-wrapper tab-content-wrapper">
            <div className="activity-wrapper">
            <div className="activity-date-wrapper">
                <h6>today</h6>
            </div>

            <div className="activity-block">
                <div className="activity-person-img">
                    <a href="#">
                        <img src="images/person.jpg" alt="person image" className="person-image"/>
                    </a>
                </div>
                <div className="activity-list-wrapper">
                    <div className="activity-list-block">
                        <p className="activity-highlight m-b-5">
                            <a href="#" className="text-capitalize bold">sohel</a> started progress on <a href="#">THIN-114 -
                                Delete
                                Records</a>
                        </p>
                        <p className="activity-log m-b-0">
                            <span className="ti-check d-task-icon bg-blue"></span> <span className="activity-time">5 hours
                                ago</span>
                        </p>
                    </div>
                </div>
            </div>

            <div className="activity-block">
                <div className="activity-person-img">
                    <a href="#">
                        <img src="images/person.jpg" alt="person image" className="person-image"/>
                    </a>
                </div>
                <div className="activity-list-wrapper">
                    <div className="activity-list-block">
                        <p className="activity-highlight m-b-5">
                            <a href="#" className="text-capitalize bold">sohel</a> logged '30 minutes' on <a href="#"> IG-63 -
                                Employer (Search Candidate)</a>
                        </p>
                        <div className="activity-content bg-skyblue">
                            <p className="m-b-0">changes upload on demo site</p>
                        </div>
                        <p className="activity-log m-b-0">
                            <span className="ti-check d-task-icon bg-blue"></span> <span className="activity-time">7 hours
                                ago</span>
                        </p>
                    </div>
                    <div className="activity-list-block">
                        <p className="activity-highlight m-b-5">
                            <a href="#" className="text-capitalize bold">sohel</a> changed the status to Planning To Do on <a href="#">THIN-114 -
                                Delete
                                Records</a>
                        </p>
                        <div className="activity-content bg-skyblue">
                            <p className="m-b-0">changes upload on demo site</p>
                        </div>
                        <p className="activity-log m-b-0">
                            <span className="ti-check d-task-icon bg-blue"></span> <span className="activity-time">8 hours
                                ago</span>
                        </p>
                    </div>
                </div>
            </div>

            <div className="activity-block">
                <div className="activity-person-img">
                    <a href="#">
                        <img src="images/person.jpg" alt="person image" className="person-image"/>
                    </a>
                </div>
                <div className="activity-list-wrapper">
                    <div className="activity-list-block">
                        <p className="activity-highlight m-b-5">
                            <a href="#" className="text-capitalize bold">sohel</a> changed the status to Done <a href="#">
                                <span className="line-through">HOYA-125</span> - Admin: Admin User: Edit: Error showing</a> with
                            a resolution of 'Done'
                        </p>
                        <div className="activity-content bg-skyblue">
                            <p className="m-b-0">changes upload on demo site</p>
                        </div>
                        <p className="activity-log m-b-0">
                            <span className="ti-control-record d-task-icon bg-orange"></span> <span className="activity-time">9
                                hours
                                ago</span>
                        </p>
                    </div>
                    <div className="activity-list-block">
                        <p className="activity-highlight m-b-5">
                            <a href="#" className="text-capitalize bold">sohel</a> updated the Description of <a href="#">KM-180
                                - API issue</a>
                        </p>
                        <div className="activity-content bg-skyblue">
                            <div className="user-content">
                                <p>1. Api Name:- user/profile</p>
                                <p>issue:- profile_pic key missing in user_details object</p>
                                <p>2. Api name:- course/edit</p>
                                <p>issue:- remove intro id validation</p>
                                <p>3. Api name:- user/dashboard</p>
                                <p>issue:- duration is missing in last uploaded video object</p>
                                <p>4. Api name:-</p> <a className="content-more" href="/browse/KM-180">Read more</a>
                            </div>
                        </div>
                        <p className="activity-log m-b-0">
                            <span className="ti-check d-task-icon bg-blue"></span> <span className="activity-time">7 hours
                                ago</span>
                        </p>
                    </div>
                </div>
            </div>

        </div>
       
    </div>
</div>
    );
}
}