
import React, { Component } from 'react'
import ReactDOM from 'react-dom';
import axios from "axios";
import {Link} from "react-router-dom";
import jqueryMin from 'jquery/dist/jquery.min';

export class Datedetails extends Component {
render (){
    let TaskDetails= this.props.TaskDetails;
 return (
    <div className="tracker-block  border-orange border-left-2 p-l-10">
        <p className="collapse-para m-b-5">
            <a className="collapse-link" data-toggle="collapse" href="#collapse-date" role="button" aria-expanded="false" aria-controls="collapseExample">
                Date<span className="ti-angle-double-right"></span>
            </a>
        </p>
        <div className="collapse show" id="collapse-date">
            <div className="collapse-wrapper">
                <div className="collapse-block">
                    <div className="collpase-lable">
                        Created :
                    </div>
                    <div className="collpase-content">
                        <p className="form-control-p">{TaskDetails.created_at}</p>
                    </div>
                </div>
                <div className="collapse-block">
                    <div className="collpase-lable">
                        Updated :
                    </div>
                    <div className="collpase-content">
                        <p className="form-control-p">{TaskDetails.updated_at}</p>
                    </div>
                </div>
             </div>
        </div>
    </div>      
    );
  }
}
    