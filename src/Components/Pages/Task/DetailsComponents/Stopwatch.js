
import React, { Component } from 'react'
import ReactDOM from 'react-dom';
import axios from "axios";
import {Link} from "react-router-dom";
import jqueryMin from 'jquery/dist/jquery.min';


export class Stopwatch extends Component {
render (){
 return (
 <div className="tracker-block  border-purple border-left-2 p-l-10">
 <p className="collapse-para m-b-5">
     <a className="collapse-link" data-toggle="collapse" href="#collapse-logs" role="button" aria-expanded="false" aria-controls="collapseExample">
         Manage Worklog<span className="ti-angle-double-right"></span>
     </a>
 </p>
 <div className="collapse show" id="collapse-logs">
     <div className="collapse-wrapper">
         <div className="jumbotron text-center">
             <div className="timer">
                 <span className="minutes">
                     00
                 </span>
                 :
                 <span className="seconds">
                     00
                 </span>
             </div>
             <div className="time-buttons">
                 <button className="btn btn-success" data-action="start">
                     Start
                 </button>
                 <button className="btn btn-danger" data-action="stop">
                     Stop
                 </button>
                 <button className="btn btn-link btn-block" data-action="reset">
                     Reset
                 </button>
             </div>
         </div>
     </div>
 </div>
</div> 
    );
}
}
