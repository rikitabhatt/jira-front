
import React, { Component } from 'react'
import ReactDOM from 'react-dom';
import axios from "axios";
import {Link} from "react-router-dom";
import jqueryMin from 'jquery/dist/jquery.min';

export class History extends Component {
render (){
 return (
<div className="tab-pane fade show" id="pills-history" role="tabpanel" aria-labelledby="pills-history-tab">
    <div className="tab-content-wrapper tab-history-wrapper">
        <div className="comments-tab-block m-b-30">
            <p className="work-log-p m-b-5">
                <a href="#">
                    <img src="images/person.jpg" alt="person image" className="person-image"/> <span className="work-log-person text-capitalize">Saniya
                        vohra</span></a> commented- <span className="logged-date-time">17/May/21 12:29 PM
                </span>
            </p>
        </div>

        <div className="comments-tab-block m-b-30">
            <p className="work-log-p m-b-5">
                <a href="#">
                    <img src="images/person.jpg" alt="person image" className="person-image"/> <span className="work-log-person text-capitalize">Saniya
                        vohra</span></a> made changes- <span className="logged-date-time">17/May/21 12:29 PM
                </span>
            </p>
            <div className="activity-content bg-skyblue">
                <div className="history-table">
                    <p className="sprint-detail m-b-10"><b>Sprint :</b> <span className="sprint-data">May1st WK 3 [ 49 ] </span>
                    </p>
                    <div className="table-responsive history-table simple-bs-table">
                        <table className="table">
                            <thead>
                                <tr>
                                    <th scope="col">Worklog Id</th>
                                    <th scope="col">Date/Time</th>
                                    <th scope="col">Original Status</th>
                                    <th scope="col">New status</th>
                                    <th scope="col">WorLog</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>[ 10102 ]</td>
                                    <td>17/May/21 11:23 AM</td>
                                    <td>Planning To Do</td>
                                    <td>To Do</td>
                                    <td>
                                        <p className="table-p">
                                            Remaining Estimate : 1hr</p>
                                        <p className="table-p">Time Spent : 1hr
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>[ 10102 ]</td>
                                    <td>17/May/21 11:23 AM</td>
                                    <td>Planning To Do</td>
                                    <td>To Do</td>
                                    <td>

                                    </td>
                                </tr>
                                <tr>
                                    <td>[ 10102 ]</td>
                                    <td>17/May/21 11:23 AM</td>
                                    <td>Planning To Do</td>
                                    <td>To Do</td>
                                    <td>
                                        <p className="table-p">
                                            Remaining Estimate : 1hr</p>
                                        <p className="table-p">Time Spent : 1hr
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>[ 10102 ]</td>
                                    <td>17/May/21 11:23 AM</td>
                                    <td>Ready For QA</td>
                                    <td>QA</td>
                                    <td>
                                        <p className="table-p">
                                            Remaining Estimate : 1hr</p>
                                        <p className="table-p">Time Spent : 1hr
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>[ 10102 ]</td>
                                    <td>17/May/21 11:23 AM</td>
                                    <td>In progress</td>
                                    <td>Done</td>
                                    <td>

                                    </td>
                                </tr>
                                <tr>
                                    <td>[ 10102 ]</td>
                                    <td>17/May/21 11:23 AM</td>
                                    <td>To Do</td>
                                    <td>In progress</td>
                                    <td>
                                        <p className="table-p">
                                            Remaining Estimate : 1hr</p>
                                        <p className="table-p">Time Spent : 1hr
                                        </p>
                                    </td>
                                </tr>


                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>

        <div className="comments-tab-block m-b-30">
            <p className="work-log-p m-b-5">
                <a href="#">
                    <img src="images/person.jpg" alt="person image" className="person-image"/> <span className="work-log-person text-capitalize">Saniya
                        vohra</span></a> commented- <span className="logged-date-time">12/April/21 10:19 PM
                </span>
            </p>
        </div>

        <div className="comments-tab-block m-b-30">
            <p className="work-log-p m-b-5">
                <a href="#">
                    <img src="images/person.jpg" alt="person image" className="person-image"/> <span className="work-log-person text-capitalize">Saniya
                        vohra</span></a> created issue- <span className="logged-date-time">07/jan/21 01:29 PM
                </span>
            </p>
        </div>
    </div>
</div>
    );
}
}