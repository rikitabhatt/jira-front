
import React, { Component } from 'react'
import ReactDOM from 'react-dom';
import axios from "axios";
import { useLocation,useHistory } from "react-router-dom";
import jqueryMin from 'jquery/dist/jquery.min';
import {  alertService } from '../../../../_services';
import EditCommentModal from '../../../Modal/EditCommentModal';
export class Comments extends Component {

    constructor(props) {
        super(props);
         this.state = {
            commentList :[],
            comment: '',
            loading: true,
        };
        this.TaskDetails = props.TaskDetails;
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);  
        this.handleToUpdate	= this.handleToUpdate.bind(this);
    }
    handleChange(event) {
        this.setState({ comment: event.target.value });
    }
	handleToUpdate(id){
        this.sync(id);
    }
    async recordDelete(id){
        const res =  await axios.delete(`http://localhost:8000/api/task_comment/${id}`);
        alertService.success('Delete commit successfully', { keepAfterRouteChange: true });
        this.sync(this.TaskDetails.id);
    }
    async handleSubmit(event) {
        event.preventDefault();
        const res = await axios.post('http://localhost:8000/api/task_comment/', {
            task_id:event.target.getAttribute('data-taskid'),
            comment:this.state.comment,
            created_by: 1
        });
        alertService.success('Comment add successfully.', { keepAfterRouteChange: true });
        this.setState({ comment: '' });
        this.sync(this.TaskDetails.id);
    }
    componentDidMount() {
        this.sync(this.TaskDetails.id);
    }
    async sync(id){ 
       await axios.post(`http://localhost:8000/api/task_comments/${id}`).then(response => { 
        console.log(response.data.data.length);
           if(response.data.data.length > 0){
            this.setState({ loading: false, commentList: response.data.data });
           }
        })
        .catch(error => {
            console.log(error.response)
        });
    }
render (){
    let isShow = true;
    if(!this.state.comment){
        isShow = false;
    }else{
        isShow = true;
    }
return (
<div className="tab-pane fade show active" id="pills-comments" role="tabpanel" aria-labelledby="pills-comments-tab">   
    <div className="tab-content-wrapper tab-comments-wrapper">
        <div className="tracker-block task-activity-block">
            <p className="collapse-para m-b-5">
                <a className="collapse-link comment-btn collapsed" data-toggle="collapse" href="#add-comment" role="button" aria-expanded="false" aria-controls="collapseExample"><span className="ti-comment"></span>
                    Comment
                </a>
            </p>
            <div className={"collapse " + (isShow === true?'show':'hide')} id="add-comment">
                <div className="collapse-wrapper">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="collapse-block">
                                <div className="collpase-content comment-editor">
                                    <form className="editor-form">
                                        <textarea name="comment" id="comment" rows="10" cols="80" onChange={this.handleChange}  value={this.state.comment}> 
                                        </textarea>
                                        <div className="btn-group-wrapper m-b-5">
                                             <button className="btn btn-green"  disabled={!this.state.comment} onClick={this.handleSubmit} data-taskid={this.TaskDetails.id}>Add</button>
                                        </div>
                                     </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {this.state.commentList.map((row, index) => (
            <div className="comments-tab-block m-b-30">
               <div className="comment-action-btns">
                    <EditCommentModal handleToUpdate = {this.handleToUpdate} className="comments-icon-list" CommentDetails={row}><span className="ti-pencil"></span></EditCommentModal>
                    <a onClick={()=>{this.recordDelete(row.id)}} data-id={row.id} className="comments-icon-list"><span className="ti-archive"></span></a>
                </div>
                <p className="work-log-p m-b-5">
                    <a href="#">
                    <img src="images/person.jpg" alt="person image" className="person-image"/> <span className="work-log-person text-capitalize">{row.createby_details.name}
                        </span></a> Commented- <span className="logged-date-time">{row.comment_time}
                        </span>
                </p>
                <div className="activity-content bg-skyblue" data-taskid={row.task_id} data-id={row.id}>
                    <div className="user-content"> {row.comment} </div>
                </div>
            </div>
        ))}
    </div>
</div>
 );
}
}
