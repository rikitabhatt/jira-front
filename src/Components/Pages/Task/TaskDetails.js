import React, { Component } from 'react'
import ReactDOM from 'react-dom';
import axios from "axios";
import {Link} from "react-router-dom";
import jqueryMin from 'jquery/dist/jquery.min';
import {  alertService } from '../../../_services'
// components add for task details page
import { Comments } from './DetailsComponents/Comments';
import { Worklog } from './DetailsComponents/Worklog';
import { Activity } from './DetailsComponents/Activity';
import { History } from './DetailsComponents/History';
import { Taskaction } from './DetailsComponents/Taskaction';
import {People} from  './DetailsComponents/People';
import { Stopwatch } from './DetailsComponents/Stopwatch';
import { Subtask } from './DetailsComponents/Subtask';
import { Datedetails} from './DetailsComponents/Datedetails';
import { Timetracking } from './DetailsComponents/Timetracking';
export class TaskDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
            results:[],
            issue_title: "",
            loading: true,
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);    
    }
    handleChange(event) {
        this.setState({ issue_title: event.target.value });
    }
    async handleSubmit(event) {
        event.preventDefault();
        const res = await axios.post('http://localhost:8000/api/task/updateTitle', {
            task_id:event.target.getAttribute('data-taskid'),
            issue_title:event.target.value,
            old_title: this.state.results.issue_title
        });
    }
    componentDidMount() {
        this.sync();
    }
    async sync(){ 
        const res =  await axios.get(`http://localhost:8000/api/tasks/getTaskBySlug/${this.props.match.params.taskSlug}`);
        this.setState({ loading: false, results: res.data.data });
    }
    render (){
        let project_details = [];
        let taskData = [];
        if(this.state.loading === false){
            taskData =this.state.results;
            project_details = taskData.project_details;
           
        }else {
            return (<div className="container-fluid">
                <div className="title-bar">
                    <div className="page-title">
                        <h2>Open issues</h2> 
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-9">Loading </div>
                </div>
            </div>);
        }
        return (
        <div className="container-fluid">
            <div className="title-bar">
                <div className="page-title">
                    <h2>Open issues</h2> 
                </div>
            </div>
            <div className="row">
                <div className="col-md-9">
                    <div className="task-info-wrapper">
                        <div className="task-title">
                            <div className="project-pic">
                                <span className="ti-flag-alt"></span>
                            </div>
                            <div className="task-title-content">
                                <div className="task-project-title">
                                    <Link className="text-capitalize" to={"/logout"}>{project_details.project_name}</Link>
                                    <Link className="text-uppercase" to={`/task_details/${taskData.task_slug}`}>{taskData.task_slug}</Link>
                                    
                                </div>
                                <div className="editable-task-title">
                                    <form className="editable-task-form">
                                        <input type="text" name="issue_title" className="form-control-h2" id="editable-title" aria-describedby="emailHelp" 
                                         onBlur={this.handleSubmit}  onChange={this.handleChange} data-taskid={taskData.id} defaultValue={taskData.issue_title} />
                                        <span className="ti-pencil title-edit-icon">Edit</span>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div className="task-main-area">
                            <Taskaction TaskDetails={taskData}></Taskaction>
                            <div className="tasks-activity-wrapper">
                                <div className="tracker-block task-detail-block">
                                    <p className="collapse-para m-b-5">
                                        <a className="collapse-link" data-toggle="collapse" href="#collapse-task-detail" role="button" aria-expanded="false" aria-controls="collapseExample">
                                            Details<span className="ti-angle-double-right"></span>
                                        </a>
                                    </p>
                                    <div className="collapse show" id="collapse-task-detail">
                                        <div className="collapse-wrapper">
                                            <div className="row">
                                                <div className="col-md-6">
                                                    <div className="collapse-block align-center">
                                                        <div className="collpase-lable">
                                                            type :
                                                        </div>
                                                        <div className="collpase-content task-detail-content">
                                                            <p className="m-b-0"><span className="ti-control-record d-task-icon bg-orange"></span>{taskData.issue_type}</p>
                                                        </div>
                                                    </div>
                                                    <div className="collapse-block align-center">
                                                        <div className="collpase-lable">
                                                            priority :
                                                        </div>
                                                        <div className="collpase-content task-detail-content">
                                                            <p className="m-b-0">{taskData.priority_name}</p>
                                                        </div>
                                                    </div>
                                                    <div className="collapse-block align-center">
                                                        <div className="collpase-lable">
                                                            Labels :
                                                        </div>
                                                        <div className="collpase-content task-detail-content">
                                                            <p className="m-b-0">none</p>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div className="col-md-6">
                                                    <div className="collapse-block align-center">
                                                        <div className="collpase-lable">

                                                            Status :
                                                        </div>
                                                        <div className="collpase-content task-detail-content">
                                                            <p className="m-b-0"><span className="status-tag text-purple border-purple">Planning to
                                                                    do</span></p>
                                                        </div>
                                                    </div>
                                                    <div className="collapse-block align-center">
                                                        <div className="collpase-lable">

                                                            Resolution :
                                                        </div>
                                                        <div className="collpase-content task-detail-content">
                                                            <p className="m-b-0">Unresolved</p>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div className="tracker-block task-activity-block">
                                    <p className="collapse-para m-b-5">
                                        <a className="collapse-link" data-toggle="collapse" href="#collapse-task-detail" role="button" aria-expanded="false" aria-controls="collapseExample">
                                            Description<span className="ti-angle-double-right"></span>
                                        </a>
                                    </p>
                                    <div className="collapse show" id="collapse-task-detail">
                                        <div className="collapse-wrapper">
                                            <div className="row">
                                                <div className="col-md-12">
                                                    <div className="collapse-block">
                                                        <div className="collpase-content task-detail-content">
                                                            <div className="user-content-block">
                                                                <p>{taskData.description}</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {/* Activity */}
                                <div class="tracker-block task-activity-block">
                                    <p class="collapse-para m-b-5">
                                        <a class="collapse-link" data-toggle="collapse" href="#collapse-task-activity" role="button" aria-expanded="false" aria-controls="collapseExample">
                                            Activity<span class="ti-angle-double-right"></span>
                                        </a>
                                    </p>
                                    <div class="collapse show" id="collapse-task-activity">
                                        <div class="collapse-wrapper">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="collapse-block">
                                                        <div class="task-activity-tabs">
                                                            <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                                                                <li class="nav-item" role="presentation">
                                                                    <a class="nav-link active" id="pills-comments-tab" data-toggle="pill" href="#pills-comments" role="tab" aria-controls="pills-comments" aria-selected="false">Comments</a>
                                                                </li>
                                                                <li class="nav-item" role="presentation">
                                                                    <a class="nav-link" id="pills-worklog-tab" data-toggle="pill" href="#pills-worklog" role="tab" aria-controls="pills-worklog" aria-selected="false">Worklog</a>
                                                                </li>
                                                                <li class="nav-item" role="presentation">
                                                                    <a class="nav-link" id="pills-history-tab" data-toggle="pill" href="#pills-history" role="tab" aria-controls="pills-history" aria-selected="false">History</a>
                                                                </li>
                                                                <li class="nav-item" role="presentation">
                                                                    <a class="nav-link" id="pills-activity-tab" data-toggle="pill" href="#pills-activity" role="tab" aria-controls="pills-activity" aria-selected="false">Activity</a>
                                                                </li>
                                                            </ul>


                                                            <div class="tab-content" id="pills-tabContent">
                                                                <Comments TaskDetails={taskData}></Comments>
                                                                <Worklog TaskDetails={taskData}></Worklog>
                                                                <History TaskDetails={taskData}></History>
                                                                <Activity TaskDetails={taskData}></Activity>
                                                            </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                             </div>
                        </div>
                    </div>                        
                </div>
                <div className="col-md-3">
                    <People TaskDetails={taskData}></People>
                    <Datedetails TaskDetails={taskData}></Datedetails>
                    <Subtask TaskDetails={taskData}></Subtask>
                    <Timetracking TaskDetails={taskData}></Timetracking>
                    <Stopwatch TaskDetails={taskData}></Stopwatch>
                </div>
            </div>
        </div>   
        );
     }
}
