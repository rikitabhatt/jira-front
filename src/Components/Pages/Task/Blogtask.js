import React, { Component } from 'react'
import ReactDOM from 'react-dom';
import axios from "axios";
import {Link} from "react-router-dom";
import jqueryMin from 'jquery/dist/jquery.min';
import {  alertService } from '../../../_services';

//process.env.REACT_APP_WEBSITE_URL
const $ = require('jquery');
$.DataTable = require('datatables.net');
export class BlogTask extends Component {
    constructor(props) {
        super(props);
        this.state = {
          
        };
    }
    
    render (){
       
     return (
        <div className="backlog-tasks">
            <div className="tracker-block task-detail-block">
                <p className="collapse-para m-b-5">
                    <a className="collapse-link" data-toggle="collapse" href="#collapse-sprint-1" role="button"
                        aria-expanded="false" aria-controls="collapseExample">
                        May WK4<span className="ti-angle-double-right"></span>
                    </a>
                    <span className="light-label">2 of 9 issues</span>
                    <span className="status-tag text-white bg-green text-uppercase">Active</span>
                </p>
                <div className="collapse show" id="collapse-sprint-1">
                    <div className="collapse-wrapper">
                        <div className="row">
                            <div className="col-md-12">
                                <div className="collapse-block">
                                    <div className="backlog-block">
                                        <p className="work-log-p m-b-5">
                                            <a  className="person-sprint" data-toggle="tooltip" data-placement="top"
                                                data-html="true" title="
                                                                <div className='tooltip-wrapper'>
                                                                <p className='pesrosnname text-capitalize'>sohel khan</p>
                                                                <p><span calss='issue-count'>0</span> issues</p>
                                                                <p><span calss='issue-count'>0</span> story points</p> </div> ">
                                                <img src="images/person.jpg" alt="person image" className="person-image"/></a>
                                            <a  className="person-sprint" data-toggle="tooltip" data-placement="top"
                                                data-html="true" title="
                                                                <div className='tooltip-wrapper'>
                                                                <p className='pesrosnname text-capitalize'>sohel khan</p>
                                                                <p><span calss='issue-count'>0</span> issues</p>
                                                                <p><span calss='issue-count'>0</span> story points</p> </div> ">
                                                <img src="images/person.jpg" alt="person image" className="person-image"/></a>
                                            <a className="person-sprint" data-toggle="tooltip" data-placement="top"
                                                data-html="true" title=" <div className='tooltip-wrapper'>
                                                                <p className='pesrosnname text-capitalize'>sohel khan</p>
                                                                <p><span calss='issue-count'>0</span> issues</p>
                                                                <p><span calss='issue-count'>0</span> story points</p> </div>  ">

                                                <img src="images/person.jpg" alt="person image" className="person-image"/></a>

                                            <a  data-toggle="modal" data-target="#sprint-popup" className="sprintpop"
                                                data-toggle="tooltip" data-placement="top" data-html="true" title="
                                                <div className='tooltip-wrapper'>
                                                                <p className='pesrosnname text-capitalize'>View summary Of Assigned Work</p>
                                                            </div>"> <span className="ti-more-alt sprint-icon"></span></a>

                                            <span className="logged-date-time light-label ">17/May/21 12:29 PM
                                            </span>-<span className="logged-date-time light-label ">17/May/21 12:29 PM</span>
                                        </p>

                                        <div className="backlog-task-list-wrapper">
                                            <ul className="backlog-task-list">
                                                {this.props.results.map((row, index) => (
                                                    <li className="backlog-li-block">
                                                        <div className="backlog-task-content">
                                                            <span className="ti-check d-task-icon bg-blue" style={{textTransform: 'capitalize'}}></span>
                                                           
                                                            <Link className="js-key-link" to={`/task_details/${row.task_slug}`}>{row.issue_title}</Link>
                                                        </div>
                                                        <div className="backlog-task-assignee d-flex">
                                                            <p className="work-log-p">
                                                                <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" data-html="true" title="" data-original-title="
                                                                    <div className='tooltip-wrapper'>
                                                                    <p className='pesrosnname text-capitalize'>Assignee : sohel khan</p>
                                                                    </div> ">
                                                                    <img src="images/person.jpg" alt="person image" className="person-image" />
                                                                </a>
                                                            </p>
                                                            <p className="task-et-p  m-l-5">
                                                                <span className="et-span">5h</span>
                                                            </p>
                                                        </div>
                                                    </li>
                                                ))}
                                                
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>   
        </div>
    );
     }
}
