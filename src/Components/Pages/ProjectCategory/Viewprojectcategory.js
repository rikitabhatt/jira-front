import React, { useState, useEffect } from "react";
import axios from "axios";
import { useHistory, useParams , Link } from "react-router-dom";

const Viewprojectcategory= () => {
  let history = useHistory();
  const { id } = useParams();
  const [projectcategory, setProjectcategory] = useState({
    project_cat_name: "",
    project_cat_description: ""
  });

  const { project_cat_name, project_cat_description } = projectcategory;
  const onInputChange = e => {
    setProjectcategory({ ...projectcategory, [e.target.name]: e.target.value });
  };

  useEffect(() => {
    loadFromData();
  }, []);
  const loadFromData = async () => {
    const result = await axios.get(`http://localhost:8000/api/project_category/${id}`);
    setProjectcategory(result.data.data);
  };
  return (
    <div className="container-fluid">
      <div className="title-bar">
          <div className="page-title">
              <h2>Project Category Management</h2>
          </div>
      </div>
      <div className="row">
          <div className="col-md-12">
              <form>
                <div className="form-group">
                <label className="form-label" htmlFor="project_cat_name">Project Category<span className="astric text-orange">*</span></label>
                  <input type="text" className="form-control form-control-lg"  name="project_cat_name" value={project_cat_name} readOnly />
                </div>
                <div className="form-group">
                <label className="form-label" htmlFor="project_cat_description">Project Category Description<span className="astric text-orange">*</span></label>
                  <input type="text" className="form-control form-control-lg"  name="project_cat_description" value={project_cat_description} readOnly />
                </div>
                <Link className="btn btn-warning btn-block" to={"/projectcategory"}>Back </Link>
              </form>
          </div>
      </div>
    </div>
  );
};
export default Viewprojectcategory;