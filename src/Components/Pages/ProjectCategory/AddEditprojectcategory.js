import React, { useState, useEffect } from "react";
import axios from "axios";
import { useHistory, useParams } from "react-router-dom";
import {  alertService } from '../../../_services';
const AddEditprojectcategory = () => {
  let history = useHistory();
  const { id } = useParams();
  const isAddMode = !id;
  const [projectcategory, setProjectcategory] = useState({
    project_cat_name: "",
    project_cat_description: "",
    errors: {},
  });

  const { project_cat_name, project_cat_description } = projectcategory;
  const onInputChange = e => {
    setProjectcategory({ ...projectcategory, [e.target.name]: e.target.value });
  };

  useEffect(() => {
    loadFromData();
  }, []);

  const onSubmit = async e => {   
    e.preventDefault();
    
    if(validateForm(projectcategory) == false)
    { 
      e.preventDefault();
      return false;
    }  
    let data = {};
    let msg ='';
    if(!isAddMode){ //edit  
        data=await axios.put(`http://localhost:8000/api/project_category/${id}`, projectcategory);
        msg = 'Edit record successfully';
    }else {// create mode ==
       data= await axios.post(`http://localhost:8000/api/project_category/`, projectcategory);
       msg = 'Add record successfully';
    }
    if(data.data.error)
    {
      setProjectcategory({
        ...projectcategory, 
        errors: data.data.error,
      });
      return false;
    }else{
      alertService.success(msg, { keepAfterRouteChange: true });
    }
    history.push("/projectcategory");
  };
  const loadFromData = async () => {
    let errors = {};
    if (!isAddMode) { //edit 
      const result = await axios.get(`http://localhost:8000/api/project_category/${id}`);
      setProjectcategory(result.data.data);
    }
  };
   // Validation function
   const validateForm = (FormData) => {
    console.log('formData : '+FormData.project_cat_name);
    let errors = {};
    let formIsValid = true;

    if (!FormData.project_cat_name) {
      formIsValid = false;
      errors['project_cat_name'] = 'Project category field is required';
    }

    if (!FormData.project_cat_description) {
      formIsValid = false;
      errors['project_cat_description'] = 'Project description field is required';
    }
    setProjectcategory({
      ...projectcategory, 
      errors: errors,
    });
    return formIsValid;
  };

  return (
    <div className="container-fluid">
      <div className="title-bar">
          <div className="page-title">
              <h2>Project Category Management</h2>
          </div>
      </div>
      <div className="row">
          <div className="col-md-12">
              <form>
                <div className="form-group">
                <label className="form-label" htmlFor="project_cat_name">Project Category<span className="astric text-orange">*</span></label>
                  <input type="text" className="form-control form-control-lg" placeholder="Enter   Name" name="project_cat_name" value={project_cat_name} onChange={e => onInputChange(e)} />
                </div>
                { projectcategory.errors &&<div className='errorMsg'>{ projectcategory.errors.project_cat_name }</div> }
                <div className="form-group">
                <label className="form-label" htmlFor="project_cat_description">Project Category Description<span className="astric text-orange">*</span></label>
                 <textarea className="form-control form-control-lg" value={project_cat_description} name="project_cat_description" onChange={e => onInputChange(e)}> </textarea>
                 { projectcategory.errors &&<div className='errorMsg'>{ projectcategory.errors.project_cat_description }</div> }
                </div>
                
                <button className="btn btn-warning btn-block" onClick={e => onSubmit(e)}>{isAddMode ? 'Save' : 'Update'} </button>
              </form>
          </div>
      </div>
    </div>
  );
};

export default AddEditprojectcategory;