import React, { Component } from 'react'
import ReactDOM from 'react-dom';
import axios from "axios";
import {Link} from "react-router-dom";
import {  alertService } from '../../../_services';
// import component files 
//Datatable Modules
import "datatables.net-dt/js/dataTables.dataTables"
import "datatables.net-dt/css/jquery.dataTables.min.css"
//import $, { css } from 'jquery';
import jqueryMin from 'jquery/dist/jquery.min';

const $ = require('jquery');
$.DataTable = require('datatables.net');
//require("datatables.net")(window, $);

export class Action extends Component {
    constructor(props) {
        super(props);
        this.state = {
            results: [],
            loading: true,
        };
        this.DataTable = null;
    }
    //option 1
    async getData() {
        const res = await axios.post("http://localhost:8000/api/moduleactions");
        console.log(res.data.data.original.data);
        this.setState({ loading: false, results: res.data.data.original.data});
    }
    componentDidMount() {
      
        this.sync();
    }
    componentWillUnmount() {
        $('.table-responsive') .find('table').DataTable().destroy(true);
      }
    recordEdit(rowid){
        this.props.history.push(`/moduleaction/edit/${rowid}`);
    }
    async recordDelete(id){
        const res =  await axios.delete(`http://localhost:8000/api/module_action/${id}`);
        alertService.success('Delete Record successfully', { keepAfterRouteChange: true });
        this.sync();
    }
    recordAdd(rowid){
        this.props.history.push(`/moduleaction/add`);
    }
    sync() {
        this.$el = $(this.el);
        this.dataTable = this.$el.DataTable({
          processing: true,
          serverSide: true,
          aLengthMenu: [[2,5,10,25, 50, 75, -1], [2,5,10,25, 50, 75, "All"]],
          iDisplayLength: 10,
            bDestroy: true,
            ajax: {
                url: 'http://localhost:8000/api/moduleactions',
                type: 'post',
                //dataSrc: "data.original.data",
                dataSrc: function(json){
                    json.draw = json.data.original.draw;
                    json.recordsTotal = json.data.original.recordsTotal;
                    json.recordsFiltered = json.data.original.recordsFiltered;
                    return json.data.original.data;
                 }
            },

          columns: [
            { title : "Name", data: "action_name" },
            { title : "Label",data: "action_label" },
            {    title : "Action",
                "data": null,
                "searchable": false,
                "orderable": false,
                "render": function (data, type, full, meta) {
                    return ''
                }
            },
          ],
          "columnDefs": [
            {
                targets: 2,
                createdCell: (td, cellData, rowData, row, col) =>
                  ReactDOM.render(
                    <div>
                        <button dataId={rowData.action_id} className="btn btn-warning" style={{borderRadius: '30px'}} onClick={(e) => this.recordEdit(rowData.action_id)}> Edit </button> 
                        <button className="btn btn-warning" style={{borderRadius: '30px'}} onClick={()=>{this.recordDelete(rowData.action_id)}}> Delete </button>
                    </div>
                    , td),
              } 
        ]
        });
      }
    render (){
     return (
         <div className="container-fluid">
        <div className="title-bar">
            <div className="page-title">
                <h2>Action Management</h2>
                <button  className="btn btn-success"  onClick={(e) => this.recordAdd()} style={{borderRadius: '30px'}}> Add </button> 
            </div>
        </div>
        <div className="row">
            <div className="col-md-12">
            <div className="backlogs-wrapper">
                        <div className="table-responsive">
                            <table id="all-projects" className="table" ref={(el) => (this.el = el)}></table>
                        </div>
                    </div>
            </div>
        </div>
    </div>
    );
     }
}
