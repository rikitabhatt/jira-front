import React, { useState, useEffect } from "react";
import axios from "axios";
import { useHistory, useParams } from "react-router-dom";
import {  alertService } from '../../../_services';
const AddEditaction = () => {
  let history = useHistory();
  const { id } = useParams();
  const isAddMode = !id;
  const [action, setAction] = useState({
    action_name: "",
    action_label: "",
    create_at:1,
    errors: {},
  });

  const { action_name, action_label, create_at } = action;
  const onInputChange = e => {
    setAction({ ...action, [e.target.name]: e.target.value });
  };

  useEffect(() => {
    loadFromData();
  }, []);

  const onSubmit = async e => {
    e.preventDefault();
    if(validateForm(action) == false)
    {
      e.preventDefault();
      return false;
    }  
    let data = {};
    let msg ='';
    if(!isAddMode){ //edit 
      data= await axios.put(`http://localhost:8000/api/module_action/${id}`, action);
      msg = 'Edit Record successfully';
    }else {// create mode 
      data= await axios.post(`http://localhost:8000/api/module_action/`, action);
      msg = 'Add Record successfully';
    }
    if(data.data.error)
    {
      setAction({
        ...action, 
        errors: data.data.error,
      });
      return false;
    }else{
      alertService.success(msg, { keepAfterRouteChange: true });
    }
    history.push("/moduleaction");
  };

  const loadFromData = async () => {
    let errors = {};
    if (!isAddMode) { //edit 
      const result = await axios.get(`http://localhost:8000/api/module_action/${id}`);
      setAction(result.data.data);
    }
  };
  // Validation function
  const validateForm = (formData) => {
    console.log(formData);
    let errors = {};
    let formIsValid = true;
    if (!formData.action_name) {
      formIsValid = false;
      errors['action_name'] = 'Please enter your Action name';
    }
    if (!formData.action_label) {
      formIsValid = false;
      errors['action_label'] = 'Please enter your Action label';
    }
    setAction({
      ...action, 
      errors: errors,
    });
    return formIsValid;
  };
  return (
    <div className="container-fluid">
      <div className="title-bar">
          <div className="page-title">
              <h2>Action Management</h2>
          </div>
      </div>
      <div className="row">
          <div className="col-md-5">
              <form className="page-form">
                <div className="form-group">
                  <input type="hidden" value= {create_at} onChange={e => onInputChange(e)}/>
                  <label className="form-label" htmlFor="action_name">Action Name<span className="astric text-orange">*</span></label>
                  <input type="text" className="form-control form-control-lg" placeholder="Enter Action Name" name="action_name" value={action_name} onChange={e => onInputChange(e)} />
                </div>
                {action.errors &&<div className='errorMsg'>{action.errors.action_name}</div>}
                <div className="form-group">
                <label className="form-label" htmlFor="action_label">Action Label<span className="astric text-orange">*</span></label>
                  <input type="text" className="form-control form-control-lg" placeholder="Enter Action Label" name="action_label" value={action_label}  onChange={e => onInputChange(e)} />
                </div>
                {action.errors &&<div className='errorMsg'>{action.errors.action_label}</div>}
                
                <button className="btn btn-warning btn-block" onClick={e => onSubmit(e)}>{isAddMode ? 'Save' : 'Update'} </button>
              </form>
          </div>
      </div>
    </div>
  );
};

export default AddEditaction;