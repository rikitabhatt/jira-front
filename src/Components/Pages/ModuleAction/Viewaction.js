import React, { useState, useEffect } from "react";
import axios from "axios";
import { useHistory, useParams , Link } from "react-router-dom";
import { getAllModules } from '../../Helper/Helper.js'; //callling helper function

const Viewaction= () => {
  let history = useHistory();
  const { id } = useParams();
  const [moduleaction, setModuleaction] = useState({
    action_name: "",
    action_label: "",
  });
  const { action_name, action_label } = moduleaction;
  const onInputChange = e => {
    setModuleaction({ ...moduleaction, [e.target.name]: e.target.value });
  };
  useEffect(() => {
    loadFromData();
  }, []);
  const loadFromData = async () => {
   const result = await axios.get(`http://localhost:8000/api/module_action/${id}`);
   setModuleaction(result.data.data);
  };
  return (
    <div className="container-fluid">
      <div className="title-bar">
          <div className="page-title">
              <h2>Action Management</h2>
          </div>
      </div>
      <div className="row">
          <div className="col-md-5">
              <form>
                <div className="form-group">
                <label className="form-label">Action Name<span className="astric text-orange">*</span></label>
                  <input type="text" className="form-control form-control-lg"  name="action_name" value={action_name} readOnly />
                </div>
                <div className="form-group">
                <label className="form-label" >Action Label<span className="astric text-orange">*</span></label>
                  <input type="text" className="form-control form-control-lg"  name="action_label" value={action_label} readOnly />
                </div>
                <Link className="btn btn-warning btn-block" to={"/moduleaction"}>Back </Link>
              </form>
          </div>
      </div>
    </div>
  );
};

export default Viewaction;