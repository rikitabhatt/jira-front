import React, { useState, useEffect } from "react";
import axios from "axios";
import { getAllProjectCategory } from '../../Helper/Helper.js';
import { useHistory, useParams , Link } from "react-router-dom";
import {  alertService } from '../../../_services';
const Viewproject= () => {
  let history = useHistory();
  const { id } = useParams();
  const [project, setProject] = useState(
    {
    project_name: "",
    project_description: "",
    project_key: "",
    project_category_id: 1,
    errors: {},
  });

  const { project_name, project_description,project_key,project_category_id } = project; 
  const [projectcatdropdwon, setProjectcatdropdwon] = useState([]);
  useEffect(() => {
    loadFromData();
  }, []);
  const loadFromData = async () => {
    const Projectcatdp = getAllProjectCategory();  
    setProjectcatdropdwon((await Projectcatdp).data.data);
    const result = await axios.get(`http://localhost:8000/api/project/${id}`);
    setProject(result.data.data);
  };
  return (
    <div className="container-fluid">
      <div className="title-bar">
          <div className="page-title">
              <h2>Project Management</h2>
          </div>
      </div>
      <div className="row">
          <div className="col-md-5">
          <form className="page-form">
          <div className="form-group">
                <label className="form-label"> Project Name<span className="astric text-orange">*</span></label>
                  <input type="text" className="form-control form-control-lg" placeholder="Enter Project Name" name="project_name" value={project_name} readOnly />
                </div>
                
                <div className="form-group">
                <label className="form-label"> Project Description<span className="astric text-orange">*</span></label>
                 <textarea className="form-control form-control-lg" value={project_description} name="project_description" readOnly> </textarea>
                 
                </div>
                <div className="form-group">
                  <label className="form-label"> Permission Category<span className="astric text-orange">*</span></label>
                    <select  name="permission_id" className="form-control mt-3" disabled>
                        {projectcatdropdwon.map((row, index) => (
                            <option value={row.project_cat_id} selected={row.project_cat_id == project_category_id}>{row.project_cat_name}</option>
                        ))}
                    </select>
                </div>
                <div className="form-group">
                <label className="form-label"> Project Key<span className="astric text-orange">*</span></label>
                <input type="text" className="form-control form-control-lg" placeholder="Enter Project Key" name="project_key" value={project_key} readOnly />
                
                </div>
                <Link className="btn btn-warning btn-block" to={"/projectcategory"}>Back </Link>
              </form>
          </div>
      </div>
    </div>
  );
};

export default Viewproject;