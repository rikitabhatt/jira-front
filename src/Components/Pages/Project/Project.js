import React, { Component } from 'react'
import ReactDOM from 'react-dom';
import axios from "axios";
import {Link} from "react-router-dom";
// import component files 
//Datatable Modules
import "datatables.net-dt/js/dataTables.dataTables"
import "datatables.net-dt/css/jquery.dataTables.min.css"
//import $, { css } from 'jquery';
import jqueryMin from 'jquery/dist/jquery.min';
import {  alertService } from '../../../_services';
const $ = require('jquery');
$.DataTable = require('datatables.net');
//require("datatables.net")(window, $);
export class Project extends Component {
    constructor(props) {
        super(props);
        this.state = {
            results: [],
            loading: true,
        };
        this.DataTable = null;
    } 
    //option 1
    async getData() {
        const res = await axios.post("http://localhost:8000/api/projects");
        console.log(res.data.data.original.data);
        this.setState({ loading: false, results: res.data.data.original.data});
    }
    componentDidMount() {
        //this.getData().then(() => this.sync());
        this.sync();
    }
    componentWillUnmount() {
        $('.table-responsive')
          .find('table')
          .DataTable()
          .destroy(true);
      }
    recordEdit(rowid){
        this.props.history.push(`/project/edit/${rowid}`);
    }
    async recordDelete(id){
        const res =  await axios.delete(`http://localhost:8000/api/project/${id}`);
        alertService.success('Delete Record successfully', { keepAfterRouteChange: true });
        this.sync();
    }
    recordAdd(){
        this.props.history.push(`/project/add`);
    }
    recordSetting(rowid){
        this.props.history.push(`/project/setting/${rowid}`);
    }
    sync() {
        this.$el = $(this.el);
        this.dataTable = this.$el.DataTable({
          processing: true,
          serverSide: true,
          aLengthMenu: [[2,5,10,25, 50, 75, -1], [2,5,10,25, 50, 75, "All"]],
          iDisplayLength: 10,
            bDestroy: true,
            ajax: {
                url: 'http://localhost:8000/api/projects',
                type: 'post',
                dataSrc: function(json){
                    json.draw = json.data.original.draw;
                    json.recordsTotal = json.data.original.recordsTotal;
                    json.recordsFiltered = json.data.original.recordsFiltered;
                    return json.data.original.data;
                 }
            },
          columns: [
            { title : "Name", data: "project_name" },
            { title : "Category", data: "project_category_name" },
            { title : "key", data: "project_key" },
            {    title : "Action",
                "data": null,
                "searchable": false,
                "orderable": false,
                "render": function (data, type, full, meta) {
                    return ''
                }
            },
          ],
          "columnDefs": [
            {
                targets: 3,
                createdCell: (td, cellData, rowData, row, col) =>
                  ReactDOM.render(
                    <div>
                        <button dataId={rowData.project_id} className="btn btn-warning" style={{borderRadius: '30px'}} onClick={(e) => this.recordEdit(rowData.project_id)}> Edit </button> 
                        <button className="btn btn-warning" style={{borderRadius: '30px'}} onClick={()=>{this.recordDelete(rowData.project_id)}}> Delete </button>
                        <button className="btn btn-warning" style={{borderRadius: '30px'}} onClick={()=>{this.recordSetting(rowData.project_id)}}> Setting </button>
                    </div>
                    , td),
              } 
        ]
        });
      }
    render (){
     return (
         <div className="container-fluid">
        <div className="title-bar">
            <div className="page-title">
                <h2>Project Management</h2>
                <button className="btn btn-success"  onClick={(e) => this.recordAdd()} style={{borderRadius: '10px'}}> Add </button> 
            </div>
        </div>
        <div className="row">
            <div className="col-md-12">
            <div className="backlogs-wrapper">
                        <div className="table-responsive">
                            <table id="all-projects" className="table" ref={(el) => (this.el = el)}></table>
                        </div>
                    </div>
            </div>
        </div>
    </div>
    );
     }
}
