import React, { useState, useEffect } from "react";
import axios from "axios";
import { useHistory, useParams } from "react-router-dom";
import { getAllProjectCategory } from '../../Helper/Helper.js';
import {  alertService } from '../../../_services';
const AddEditproject = () => { 
 
  let history = useHistory();
  const { id } = useParams();
  const isAddMode = !id;
  const [project, setProject] = useState(
    {
    project_name: "",
    project_description: "",
    project_key: "",
    project_category_id: 1,
    errors: {},
  });
  const [projectcatdropdwon, setProjectcatdropdwon] = useState([]);
  const { project_name, project_description,project_key,project_category_id } = project;
  const onInputChange = e => {
    setProject({ ...project, [e.target.name]: e.target.value });
  };

  useEffect(() => {
    loadFromData();
  }, []);
  const onSubmit = async e => {   
    e.preventDefault();
    
    if(validateForm(project) == false)
    { 
      e.preventDefault();
      return false;
    }  
    let data = {};
  
    let msg ='';
    if(!isAddMode){ //edit    
      data= await axios.put(`http://localhost:8000/api/project/${id}`, project);
      msg = 'Edit record successfully';
    }else {// create mode  
       data= await axios.post(`http://localhost:8000/api/project/`, project);
       msg = 'Add record successfully';
    }
    if(data.data.error)
    {
      setProject({
        ...project, 
        errors: data.data.error,
      });
      return false;
    }else{
      alertService.success(msg, { keepAfterRouteChange: true });
    }
    history.push("/project");
  };
  const loadFromData = async () => {

    const Projectcatdp = getAllProjectCategory();  
    setProjectcatdropdwon((await Projectcatdp).data.data);
   
    
    if (!isAddMode) { 
      //edit 
      const result = await axios.get(`http://localhost:8000/api/project/${id}`);
      setProject(result.data.data);
    }
  };
   // Validation function
   const validateForm = (FormData) => {
   
    let errors = {};
    let formIsValid = true;

    if (!FormData.project_name) {
      formIsValid = false;
      errors['project_name'] = 'Project Name field is required';
    }

    if (!FormData.project_description) {
      formIsValid = false;
      errors['project_description'] = 'Project description field is required';
    }
    if (!FormData.project_key) {
      formIsValid = false;
      errors['project_key'] = 'Project key field is required';
    }
    if (!FormData.project_category_id) {
      formIsValid = false;
      errors['project_category_id'] = 'Project Category field is required';
    }
    setProject({
      ...project, 
      errors: errors,
    });
    return formIsValid;
  };

  return (
    
    <div className="container-fluid">
      <div className="title-bar">
          <div className="page-title">
              <h2>Project Management</h2>
          </div>
      </div>
      <div className="row">
          <div className="col-md-5">
          <form className="page-form">
                <div className="form-group">
                <label className="form-label"> Project Name<span className="astric text-orange">*</span></label>
                  <input type="text" className="form-control form-control-lg" placeholder="Enter Project Name" name="project_name" value={project_name} onChange={e => onInputChange(e)} />
                </div>
                { project.errors &&<div className='errorMsg'>{ project.errors.project_name }</div> }
                <div className="form-group">
                <label className="form-label"> Project Description<span className="astric text-orange">*</span></label>
                 <textarea className="form-control form-control-lg" value={project_description} name="project_description" onChange={e => onInputChange(e)}> </textarea>
                 { project.errors &&<div className='errorMsg'>{ project.errors.project_description }</div> }
                </div>
                <div className="form-group">
                  <label className="form-label"> Permission Category<span className="astric text-orange">*</span></label>
                    <select  name="permission_id" className="form-control mt-3" onChange={e => onInputChange(e)}>
                        {projectcatdropdwon.map((row, index) => (
                            <option value={row.project_cat_id} selected={row.project_cat_id == project_category_id}>{row.project_cat_name}</option>
                        ))}
                    </select>
                </div>
                <div className="form-group">
                <label className="form-label"> Project Key<span className="astric text-orange">*</span></label>
                <input type="text" className="form-control form-control-lg" placeholder="Enter Project Key" name="project_key" value={project_key} onChange={e => onInputChange(e)} />
                  { project.errors &&<div className='errorMsg'>{ project.errors.project_key }</div> }
                </div>
                <button className="btn btn-warning btn-block" onClick={e => onSubmit(e)} >{isAddMode ? 'Save' : 'Update'} </button>
              </form>
          </div>
      </div>
    </div>
  );
};

export default AddEditproject;