import React from 'react'
export const Dashboard = () => {
    
    return (
        <div className="row">
            <div className="col-lg-6">
                <div className="row">
                    <div className="col-md-12">
                        <div className="dashboard-block-wrapper m-b-50">
                            <div className="dashboard-issue-data">
                                <div className="dashboard-block-title border-light-green">
                                    <h6>My issues</h6>
                                </div>
                                <div className="table-responsive">
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-12">
                        <div className="dashboard-block-wrapper m-b-50">
                            <div className="dashboard-issue-data">
                                <div className="dashboard-block-title border-blue">
                                    <h6>Projects</h6>
                                </div>
                                <div className="table-responsive">
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="col-lg-6">
                <div className="dashboard-block-wrapper m-b-50">
                    <div className="dashboard-issue-data">
                        <div className="dashboard-block-title border-orange">
                            <h6>Activity Stream</h6>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    )
}
