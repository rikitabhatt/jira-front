import React, { useState, useEffect } from "react";
import axios from "axios";
import { useHistory, useParams } from "react-router-dom";
import { getAllRole, getAllPermission } from '../../Helper/Helper.js'; 
import {  alertService } from '../../../_services';
const AddEditrolepermission = () => {
  let history = useHistory();
  const { id } = useParams();
  const [rolepermission, setRolepermission] = useState({
    permission_id: "",
    role_id: "",
    create_at: 1,
    errors:{}
  });
  const isAddMode = !id;
  const [roledropdwon, setRoledropdwon] = useState([]);
  const [permissiondropdwon, setPermissiondropdwon] = useState([]);
  const { permission_id, role_id, create_at } = rolepermission;
  const onInputChange = e => {
    setRolepermission({ ...rolepermission, [e.target.name]: e.target.value });
  };

  useEffect(() => {
    loadFromData();
  }, []);
  const onSubmit = async e => {
    e.preventDefault();
    if(validateForm(rolepermission) == false)
    {
      e.preventDefault();
      return false;
    }  
    let data = {};
    let msg ='';
    if(!isAddMode){ 
      data= await axios.put(`http://localhost:8000/api/role_permission/${id}`, rolepermission);
      msg = 'Edit record successfully';
    }else {
      data= await axios.post(`http://localhost:8000/api/role_permission/`, rolepermission);
      msg = 'Add record successfully';
     }
     if(data.data.error)
      {
        setRolepermission({
          ...rolepermission, 
          errors: data.data.error,
        });
        return false;
      }else{
        alertService.success(msg, { keepAfterRouteChange: true });
      }
    history.push("/rolepermission");
  };
  const loadFromData = async () => {
    const Roledp = getAllRole();  
    setRoledropdwon((await Roledp).data.data);

    const PermissionModuledp = getAllPermission();
    setPermissiondropdwon((await PermissionModuledp).data.data);
    if (!isAddMode) { //edit 
      const result = await axios.get(`http://localhost:8000/api/role_permission/${id}`);
      setRolepermission(result.data.data);
    } 
  };
  // Validation function
  const validateForm = (FormData) => {
    let errors = {};
    let formIsValid = true;
    if (!FormData.role_id) {
      formIsValid = false;
      errors['role_id'] = 'Please enter your Company name';
    }
    if (!FormData.permission_id) {
      formIsValid = false;
      errors['permission_id'] = 'Please enter your Company url';
    }
    setRolepermission({
      ...rolepermission, 
      errors: errors,
    });
    return formIsValid;
  };
  return (
    <div className="container-fluid">
      <div className="title-bar">
          <div className="page-title">
              <h2>Role Permission Management</h2>
          </div>
      </div>
      <div className="row">
          <div className="col-md-5">
              <form className="page-form">
              <div className="form-group">
                <label> Role Name</label>
                <select name="role_id" className="form-control mt-3" onChange={e => onInputChange(e)}>
                {!isAddMode? '': <option value="">=== Select ===</option>}
                    {roledropdwon.map((row, index) => (
                      <option value={row.role_id} selected={row.role_id == role_id}>{row.role_label}</option>
                    ))}
                </select>
                </div>
                {rolepermission.errors &&<div className='errorMsg'>{rolepermission.errors.role_id}</div>}
                <div className="form-group">
                  <label> Permission Name</label>
                    <select  name="permission_id" className="form-control mt-3" onChange={e => onInputChange(e)}>
                    {!isAddMode? '': <option value="">=== Select ===</option>}
                        {permissiondropdwon.map((row, index) => (
                            <option value={row.permission_id} selected={row.permission_id == permission_id}>{row.title}</option>
                        ))}
                    </select>
                </div>
                {rolepermission.errors &&<div className='errorMsg'>{rolepermission.errors.permission_id}</div>}
                <button className="btn btn-warning btn-block" onClick={e => onSubmit(e)}>{isAddMode ? 'Save' : 'Update'} </button>
              </form>
          </div>
      </div>
    </div>
  );
};

export default AddEditrolepermission;