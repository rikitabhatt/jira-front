import React, { useState, useEffect } from "react";
import axios from "axios";
import { useHistory, useParams , Link } from "react-router-dom";

const Viewrole= () => {
  let history = useHistory();
  const { id } = useParams();
  const [role, setRole] = useState({
    role_name: "",
    role_label: ""
  });

  const { role_name, role_label } = role;
  const onInputChange = e => {
    setRole({ ...role, [e.target.name]: e.target.value });
  };

  useEffect(() => {
    loadFromData();
  }, []);
  const loadFromData = async () => {
    const result = await axios.get(`http://localhost:8000/api/role/${id}`);
    setRole(result.data.data);
  };
  return (
    <div className="container-fluid">
      <div className="title-bar">
          <div className="page-title">
              <h2>Role Management</h2>
          </div>
      </div>
      <div className="row">
          <div className="col-md-12">
              <form>
                <div className="form-group">
                  <label>Role Name</label>
                  <input type="text" className="form-control form-control-lg"  name="role_name" value={role_name} readOnly />
                </div>
                <div className="form-group">
                <label>Role Label</label>
                  <input type="text" className="form-control form-control-lg"  name="role_label" value={role_label} readOnly />
                </div>
               
                
                <Link className="btn btn-warning btn-block" to={"/role"}>Back </Link>
              </form>
          </div>
      </div>
    </div>
  );
};

export default Viewrole;