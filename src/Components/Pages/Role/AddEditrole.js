import React, { useState, useEffect } from "react";
import axios from "axios";
import { useHistory, useParams } from "react-router-dom";
import {  alertService } from '../../../_services';
const AddEditrole = () => {
  let history = useHistory();
  const { id } = useParams();
  const isAddMode = !id;
  const [role, setRole] = useState({
    role_name: "",
    role_label: "",
    create_at:1,
    errors: {}
  });

  const { role_name, role_label, create_at } = role;
  const onInputChange = e => {
    setRole({ ...role, [e.target.name]: e.target.value });
  };

  useEffect(() => {
    loadFromData();
  }, []);

  const onSubmit = async e => {
    e.preventDefault();
    if(validateForm(role) == false)
    {
      e.preventDefault();
      return false;
    }  
    let data = {};
    let msg;
    if(!isAddMode){ //edit 
      data= await axios.put(`http://localhost:8000/api/role/${id}`, role);
      msg = 'Edit Record successfully';
    }else { // create mode 
      data= await axios.post(`http://localhost:8000/api/role/`, role);
      msg = 'Add Record successfully';
    }
    if(data.data.error)
    {
      setRole({
        ...role, 
        errors: data.data.error,
      });
      return false;
    }else{
      alertService.success(msg, { keepAfterRouteChange: true });
    }
    history.push("/role");
  };

  const loadFromData = async () => {
    if (!isAddMode) { //edit 
      const result = await axios.get(`http://localhost:8000/api/role/${id}`);
      setRole(result.data.data);
    }
  
    
  };
   // Validation function
   const validateForm = (formData) => {
    console.log(formData);
    let errors = {};
    let formIsValid = true;
    if (!formData.role_name) {
      formIsValid = false;
      errors['role_name'] = 'Please enter your Role Name';
    }
    if (!formData.role_label) {
      formIsValid = false;
      errors['role_label'] = 'Please enter your Role Label';
    }
    setRole({
      ...role, 
      errors: errors,
    });
    return formIsValid;
  };
  return (
    <div className="container-fluid">
      <div className="title-bar">
          <div className="page-title">
              <h2>Role Management</h2>
          </div>
      </div>
      <div className="row">
         
          <div className="col-md-5">
              <form className="page-form">
                <div className="form-group">
                  <input type="hidden" value= {create_at} onChange={e => onInputChange(e)}/>
                  <label className="form-label" htmlFor="open_time">Role Name<span className="astric text-orange">*</span></label>
                  <input type="text" className="form-control form-control-lg" placeholder="Enter Role Name" name="role_name" value={role_name} onChange={e => onInputChange(e)} />
                  {role.errors &&<div className='errorMsg'>{role.errors.role_name}</div>}
                </div>
                <div className="form-group">
                  <label className="form-label" htmlFor="open_time">Role Label<span className="astric text-orange">*</span></label>
                  <input type="text" className="form-control form-control-lg" placeholder="Enter Role Label" name="role_label" value={role_label}  onChange={e => onInputChange(e)} />
                  {role.errors &&<div className='errorMsg'>{role.errors.role_label}</div>}
                </div>
                
                <button className="btn btn-warning btn-block" onClick={e => onSubmit(e)}>{isAddMode ? 'Save' : 'Update'} </button>
              </form>
          </div>
      </div>
    </div>
  );
};

export default AddEditrole;