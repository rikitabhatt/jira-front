import React, { Component } from 'react';
import { Redirect } from 'react-router';
import axios from 'axios';
export default class Logout extends Component {
    state = {
        redirect: false,
    };

    componentDidMount() {
      axios.get('http://localhost:8000/api/logout/')
      .then((response) => {
        localStorage.removeItem("users")
        localStorage.removeItem("logintoken")
        this.setState({ redirect: true });
        window.location.reload();
      })
      .catch((err) => {
      })
    }

    render() {
        return this.state.redirect ?
            <Redirect to={'/'} /> :
            null;
    }
}