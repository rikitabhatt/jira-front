import React from 'react'
import { Link, NavLink } from "react-router-dom";
import {useState} from 'react';
import axios from 'axios';
import { useHistory, useParams } from "react-router-dom";
import {  alertService } from '../../_services';
const Login=({ setToken })=>{
    let history = useHistory();
    const [msg,setMsg] = useState('');
    const [username, setUsername] = useState("");
    const [pass, setPass] = useState("");
    const [user, setUser] = useState({
        user_email: "",
        password:"",
      });
      const {user_email,password} = user;
      const onInputChange = e => {
        setUser({ ...user, [e.target.name]: e.target.value });
      };

    const signIn = () =>
    {
      let isvalid = true;
      const users = { username };  // To Store Email in Localstore and send to Home Page 

       if(user.user_email === '')
       {
         alert('Email Field is empty');
         isvalid = false;
       }
       else if(user.password === '')
       {
         alert('Pass Field is empty');
         isvalid = false;
       }
      if(isvalid == true){
      axios.post('http://localhost:8000/api/login/',user)
      .then((response) => {
        console.log(response.data);
          if(response.data.status === false){
            alertService.error('Not allow', { keepAfterRouteChange: false });
          }else{
            let tokenFrom = response.data.data.token;
            let userName = response.data.data.name;
            if(tokenFrom !='' && userName != '' ){
            localStorage.setItem("users",userName);
            localStorage.setItem("logintoken",tokenFrom);
            //alert(localStorage.getItem('logintoken'));
            window.location.reload();
            //history.push('/dashboard');
            //this.props.history.push('/dashboard');
          }else{
            alertService.error('Something wrong', { keepAfterRouteChange: false });
          }
          }
       })
      .catch((err) => {
        setMsg('Error in The Code');
      })
    }
  }
    return(
        <div>
          <input name="user_email" value={user_email}  onChange={e => onInputChange(e)} placeholder='Enter Email' type='text' required />
          <input name="password" value={password}  onChange={e => onInputChange(e)} placeholder='Enter password' type='password' required/>
          <button type='submit' onClick={signIn} color='primary' variant="contained"  >Sign in</button>
        </div>
       
    )
}

export default Login