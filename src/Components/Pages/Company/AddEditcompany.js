import React, { useState, useEffect } from "react";
import axios from "axios";
import { useHistory, useParams } from "react-router-dom";
import {  alertService } from '../../../_services';
const AddEditcompany = () => {
  let history = useHistory();
  const { id } = useParams();
  const isAddMode = !id;
  const [company, setCompany] = useState({
    company_name: "",
    company_url: "",
    company_email: "",
    company_phone: "",
    open_time: "",
    monday: "",
    tuesday: "",
    wednesday: "",
    thursday: "",
    friday: "",
    saturday: "",
    sunday: "",
    errors: {},
  
  });

  const { company_name, company_url, company_email, company_phone, open_time, monday, tuesday, wednesday, thursday, friday, saturday , sunday  } = company;
  const onInputChange = e => {
    setCompany({ ...company, [e.target.name]: e.target.value });
  };

  useEffect(() => {
    loadFromData();
  }, []);

  const onSubmit = async e => {
    e.preventDefault();
   
    if(validateForm(company) == false)
    {
      e.preventDefault();
      return false;
    }  
    let data = {};
    let msg ='';
    if(!isAddMode){ 
      //edit 
      data= await axios.put(`http://localhost:8000/api/company/${id}`, company);
      msg = 'Edit record successfully';
    }else {
      // create mode 
      data= await axios.post(`http://localhost:8000/api/company/`, company);
      msg = 'Add record successfully';
    }

    if(data.data.error)
    {
      setCompany({
        ...company, 
        errors: data.data.error,
      });
      return false;
    }else{
      alertService.success(msg, { keepAfterRouteChange: true });
    }

    history.push("/company");
  };

  const loadFromData = async () => {
    let errors = {};
    if (!isAddMode) { //edit 
      const result = await axios.get(`http://localhost:8000/api/company/${id}`);
      setCompany(result.data.data);
    }
  };

  const validateForm = (companyData) => {
    console.log('company-data', companyData);
    let errors = {};
    let formIsValid = true;

    if (!companyData.company_name) {
      formIsValid = false;
      errors['company_name'] = 'Please enter your Company name';
    }

    if (!companyData.company_url) {
      formIsValid = false;
      errors['company_url'] = 'Please enter your Company url';
    }

    if (!companyData.company_email) {
      formIsValid = false;
      errors['company_email'] = 'Please enter your Company email';
    }

    if (!companyData.company_phone) {
      formIsValid = false;
      errors['company_phone'] = 'Please enter your Company phone';
    }

    if (!companyData.open_time) {
      formIsValid = false;
      errors['open_time'] = 'Please enter your open_time';
    }

    setCompany({
      ...company, 
      errors: errors,
    });

    return formIsValid;
  };

  return (
    <div className="container-fluid">
      <div className="title-bar">
          <div className="page-title">
              <h2>Company Management</h2>
          </div>
      </div>
      <div className="row">
          <div className="col-md-5">
              <form className="page-form">
                <div className="form-group">
                  <label className="form-label" htmlFor="company_name">Company Name<span className="astric text-orange">*</span></label>
                  <input type="text" className="form-control form-control-lg" placeholder="Enter Your Name" name="company_name" value={company_name} onChange={e => onInputChange(e)} />
                </div> 
                {company.errors &&<div className='errorMsg'>{company.errors.company_name}</div>}
                <div className="form-group">
                  <label className="form-label" htmlFor="company_url">Company Url<span className="astric text-orange">*</span></label>
                  <input type="text" className="form-control form-control-lg" placeholder="Comapny URL" name="company_url" value={company_url}  onChange={e => onInputChange(e)} />
                </div>
                {company.errors &&<div className='errorMsg'>{company.errors.company_url}</div>}

                <div className="form-group">
                  <label className="form-label" htmlFor="company_email">Company Email<span className="astric text-orange">*</span></label>
                  <input type="email" className="form-control form-control-lg" placeholder="Enter Your E-mail Address" name="company_email" value={company_email} onChange={e => onInputChange(e)} />
                </div>
                {company.errors &&<div className='errorMsg'>{company.errors.company_email}</div>}
                <div className="form-group">
                  <label className="form-label" htmlFor="company_phone">Company Phone<span className="astric text-orange">*</span></label>
                  <input type="text" className="form-control form-control-lg" placeholder="Enter Your Phone Number" name="company_phone" value={company_phone} onChange={e => onInputChange(e)} />
                </div>
                {company.errors &&<div className='errorMsg'>{company.errors.company_phone}</div>}

                <div className="form-group">
                  <label className="form-label" htmlFor="open_time">Company Opening time<span className="astric text-orange">*</span></label>
                  <input type="text" className="form-control form-control-lg" placeholder="Enter Your Open Time" name="open_time" value={open_time} onChange={e => onInputChange(e)} />
                </div>
                {company.errors &&<div className='errorMsg'>{company.errors.open_time}</div>}
                <button className="btn btn-warning btn-block" onClick={e => onSubmit(e)}>{isAddMode ? 'Save' : 'Update'} </button>
              </form>
          </div>
      </div>
    </div>
  );
};

export default AddEditcompany;