import React, { Component } from 'react'
import ReactDOM from 'react-dom';
import axios from "axios";
import {Link} from "react-router-dom";
import "datatables.net-dt/js/dataTables.dataTables"
import "datatables.net-dt/css/jquery.dataTables.min.css"
//import $, { css } from 'jquery';
import jqueryMin from 'jquery/dist/jquery.min';
import {  alertService } from '../../../_services';

import * as  ConstantVariable from '../../ConstantVariable/ConstantVariable.js';
//process.env.REACT_APP_WEBSITE_URL
const $ = require('jquery');
$.DataTable = require('datatables.net');
//require("datatables.net")(window, $);


export class Company extends Component {
    constructor(props) {
        super(props);
        this.state = {
            results: [],
            loading: true,
        };
    }
    //option 1
    async getData() {
        const res = await axios.get("http://localhost:8000/api/companies");
        this.setState({ loading: false, results: res.data.data });
    }

    componentDidMount() {
        // this.getData().then(() => this.sync());
        this.sync();
    }
    componentWillUnmount(){

    }
    recordEdit(rowid){
        this.props.history.push(`/company/edit/${rowid}`);
    }
    async recordDelete(id){
        const res =  await axios.delete(`http://localhost:8000/api/company/${id}`);
        alertService.success('Delete Record successfully', { keepAfterRouteChange: true });
        this.sync();
    }
    recordAdd(rowid){
        this.props.history.push(`/company/add`);
    }
    sync() {
       
        this.$el = $(this.el);
        this.$el.DataTable({
            processing: true,
            serverSide: true,
            aLengthMenu: [[2,5,25, 50, 75, -1], [2,5,25, 50, 75, "All"]],
            iDisplayLength: 10,
            bDestroy: true,
            ajax: {
                url: 'http://localhost:8000/api/companies',
                type: 'post',
                dataSrc: function(json){
                    json.draw = json.data.original.draw;
                    json.recordsTotal = json.data.original.recordsTotal;
                    json.recordsFiltered = json.data.original.recordsFiltered;
                    return json.data.original.data;
                 }
            },
          columns: [
            { title : "Name", data: "company_name" },
            { title : "Email",data: "company_email" },
            { title : "Url", data: "company_url" },
            { title : "Phone", data: "company_phone"},
            {"title" : "Action",
                "data": null,
                "searchable": false,
                "orderable": false,
                "render": function (data, type, full, meta) { return ''}
            },
          ],
          "columnDefs": [
            {
                targets: 4,
                createdCell: (td, cellData, rowData, row, col) =>
                  ReactDOM.render(
                    <div>
                        <button dataId={rowData.company_id} className="btn btn-warning" style={{borderRadius: '30px'}} onClick={(e) => this.recordEdit(rowData.company_id)}> Edit </button> 
                        <button className="btn btn-warning" style={{borderRadius: '30px'}} onClick={()=>{this.recordDelete(rowData.company_id)}}> Delete </button>
                    </div>
                    , td),
              } 
        ]
        });
      }
    render (){
     return (
         <div className="container-fluid">
        <div className="title-bar">
            <div className="page-title">
                <h2>Company Management</h2>
                <button  className="btn btn-success"  onClick={(e) => this.recordAdd()} style={{borderRadius: '30px'}}> Add </button> 
            </div>
        </div>
        <div className="row">
            <div className="col-md-12">
            <div className="backlogs-wrapper">
                        <div className="table-responsive">
                            <table id="all-projects" className="table" ref={(el) => (this.el = el)}></table>
                        </div>
                    </div>
            </div>
        </div>
    </div>
    );
     }
}
