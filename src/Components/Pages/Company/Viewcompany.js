import React, { useState, useEffect } from "react";
import axios from "axios";
import { useHistory, useParams , Link } from "react-router-dom";

const Viewcompany = () => {
  let history = useHistory();
  const { id } = useParams();
  const [company, setCompany] = useState({
    company_name: "",
    company_url: "",
    company_email: "",
    company_phone: "",
    open_time: "",
    monday: "",
    tuesday: "",
    wednesday: "",
    thursday: "",
    friday: "",
    saturday: "",
    sunday: "",
  });

  const { company_name, company_url, company_email, company_phone, open_time, monday, tuesday, wednesday, thursday, friday, saturday , sunday  } = company;
  const onInputChange = e => {
    setCompany({ ...company, [e.target.name]: e.target.value });
  };

  useEffect(() => {
    loadFromData();
  }, []);

  const onSubmit = async e => {
    e.preventDefault();
    await axios.put(`http://localhost:8000/api/company/${id}`, company);
    history.push("/company");
  };

  const loadFromData = async () => {
    const result = await axios.get(`http://localhost:8000/api/company/${id}`);
    setCompany(result.data.data);
  };
  return (
    <div className="container-fluid">
      <div className="title-bar">
          <div className="page-title">
              <h2>Company Management</h2>
          </div>
      </div>
      <div className="row">
          <div className="col-md-5">
              <form>
                <div className="form-group">
                  <label className="form-label" htmlFor="company_name">Company Name<span className="astric text-orange">*</span></label>
                  <input type="text" className="form-control form-control-lg"  name="company_name" value={company_name} readOnly />
                </div>
                <div className="form-group">
                <label className="form-label" htmlFor="company_url">Company URL<span className="astric text-orange">*</span></label>
                  <input type="text" className="form-control form-control-lg"  name="company_url" value={company_url} readOnly />
                </div>
                <div className="form-group">
                <label className="form-label" htmlFor="company_url">Company Email<span className="astric text-orange">*</span></label>
                  <input type="email" className="form-control form-control-lg"  name="company_email" value={company_email} readOnly  />
                </div>
                <div className="form-group">
                <label className="form-label" htmlFor="company_url">Company Phone<span className="astric text-orange">*</span></label>
                  <input type="text" className="form-control form-control-lg"  name="company_phone" value={company_phone} readOnly />
                </div>
                
                <Link className="btn btn-warning btn-block" to={"/company"}>Back </Link>
              </form>
          </div>
      </div>
    </div>
  );
};

export default Viewcompany;