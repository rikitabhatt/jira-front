import React, { useState, useEffect } from "react";
import axios from "axios";
import { useHistory, useParams , Link } from "react-router-dom";

const Viewmodule= () => {
  let history = useHistory();
  const { id } = useParams();
  const [module, setModule] = useState({
    module_name: "",
    module_label: ""
  });

  const { module_name, module_label } = module;
  const onInputChange = e => {
    setModule({ ...module, [e.target.name]: e.target.value });
  };

  useEffect(() => {
    loadFromData();
  }, []);
  const loadFromData = async () => {
    const result = await axios.get(`http://localhost:8000/api/module/${id}`);
    setModule(result.data.data);
  };
  return (
    <div className="container-fluid">
      <div className="title-bar">
          <div className="page-title">
              <h2>Module Management</h2>
          </div>
      </div>
      <div className="row">
          <div className="col-md-5">
              <form>
                <div className="form-group">
                <label className="form-label" >Module Name<span className="astric text-orange">*</span></label>
                  <input type="text" className="form-control form-control-lg"  name="module_name" value={module_name} readOnly />
                </div>
                <div className="form-group">
                <label className="form-label" >Module Label<span className="astric text-orange">*</span></label>
                  <input type="text" className="form-control form-control-lg"  name="module_label" value={module_label} readOnly />
                </div>
                <Link className="btn btn-warning btn-block" to={"/module"}>Back </Link>
              </form>
          </div>
      </div>
    </div>
  );
};

export default Viewmodule;