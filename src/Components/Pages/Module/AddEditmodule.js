import React, { useState, useEffect } from "react";
import axios from "axios";
import { useHistory, useParams } from "react-router-dom";
import {  alertService } from '../../../_services';
const AddEditmodule = () => {
  let history = useHistory();
  const { id } = useParams();
  const isAddMode = !id;
  const [module, setModule] = useState({
    module_name: "",
    module_label: "",
    create_at:1,
    errors: {},
  });

  const { module_name, module_label, create_at } = module;
  const onInputChange = e => {
    setModule({ ...module, [e.target.name]: e.target.value });
  };

  useEffect(() => {
    loadFromData();
  }, []);

  const onSubmit = async e => {
    //let errors = {};
    e.preventDefault();
    if(validateForm(module) == false)
    {
      e.preventDefault();
      return false;
    }  
    let data = {};
    let msg;
    if(!isAddMode){ //edit 
      data= await axios.put(`http://localhost:8000/api/module/${id}`, module);
      msg = 'Edit Record successfully';
    }else {// create mode 
      data= await axios.post(`http://localhost:8000/api/module/`, module);
      msg = 'Add Record successfully';
    }
    if(data.data.error)
    {
      setModule({
        ...module, 
        errors: data.data.error,
      });
      return false;
    }else{
      alertService.success(msg, { keepAfterRouteChange: true });
    }
    history.push("/module");
  };
  const loadFromData = async () => {
   let errors = {};
    if (!isAddMode) { //edit 
      const result = await axios.get(`http://localhost:8000/api/module/${id}`);
      setModule(result.data.data);
    }
  };
   // Validation function
   const validateForm = (formData) => {
    console.log(formData);
    let errors = {};
    let formIsValid = true;
    if (!formData.module_name) {
      formIsValid = false;
      errors['module_name'] = 'Please enter your Module Name';
    }
    if (!formData.module_label) {
      formIsValid = false;
      errors['module_label'] = 'Please enter your Module Label';
    }
    setModule({
      ...module, 
      errors: errors,
    });
    return formIsValid;
  };

  return (
    <div className="container-fluid">
      <div className="title-bar">
          <div className="page-title">
              <h2>Module Management</h2>
          </div>
      </div>
      <div className="row">
          <div className="col-md-5">
              <form className="page-form">
                <div className="form-group">
                  <input type="hidden" value= {create_at} onChange={e => onInputChange(e)}/>
                  <label className="form-label" htmlFor="module_name">Module Name<span className="astric text-orange">*</span></label>
                  <input type="text" className="form-control form-control-lg" placeholder="Enter Module Name" name="module_name" value={module_name} onChange={e => onInputChange(e)} />
                </div>
                {module.errors &&<div className='errorMsg'>{module.errors.module_name}</div>}
                <div className="form-group">
                  <label className="form-label" htmlFor="module_label">Module Label<span className="astric text-orange">*</span></label>
                  <input type="text" className="form-control form-control-lg" placeholder="Enter Module Label" name="module_label" value={module_label}  onChange={e => onInputChange(e)} />
                </div>
                {module.errors &&<div className='errorMsg'>{module.errors.module_label}</div>}
                <button className="btn btn-warning btn-block" onClick={e => onSubmit(e)}>{isAddMode ? 'Save' : 'Update'} </button>
              </form>
          </div>
      </div>
    </div>
  );
};
export default AddEditmodule;