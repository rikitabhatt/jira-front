import React, { useState, useEffect } from "react";
import axios from "axios";
import { useHistory, useParams } from "react-router-dom";
import { getAllRole, getAllUser } from '../../Helper/Helper.js'; //callling helper function
import {  alertService } from '../../../_services';

const AddEdituserrole = () => {
  let history = useHistory();
  const { id } = useParams();
  const [userrole, setUserrole] = useState({
    user_id: "",
    role_id: "",
    create_at: 1,
    
  });
  const isAddMode = !id;
  const [roledropdwon, setRoledropdwon] = useState([]);
  const [userdropdwon, setUserdropdown] = useState([]);
  const { user_id, role_id, create_at } = userrole;
  const onInputChange = e => {
    setUserrole({ ...userrole, [e.target.name]: e.target.value });
  };

  useEffect(() => {
    loadFromData();
  }, []);
  const onSubmit = async e => {
    e.preventDefault();
   
    if(validateForm(userrole) == false)
    {
      e.preventDefault();
      return false;
    }  
    let data = {};
    let msg ='';
    if(!isAddMode){ 
      //edit
      data= await axios.put(`http://localhost:8000/api/user_role/${id}`, userrole);
      msg = 'Edit record successfully';
    }else {
      // create mode 
      data= await axios.post(`http://localhost:8000/api/user_role/`, userrole);
      msg = 'Add record successfully';
     }
     if(data.data.error)
    {
      setUserrole({
        ...userrole, 
        errors: data.data.error,
      });
      return false;
    }else{
      alertService.success(msg, { keepAfterRouteChange: true });
    }
    history.push("/userrole");
  };
  const loadFromData = async () => {
    const Roledp = getAllRole();  
    setRoledropdwon((await Roledp).data.data);
    const Userdp = getAllUser();
    setUserdropdown((await Userdp).data.data);
    if (!isAddMode) { //edit 
      const result = await axios.get(`http://localhost:8000/api/user_role/${id}`);
      setUserrole(result.data.data);
    } 
  };
  const validateForm = (formData) => {
   
    let errors = {};
    let formIsValid = true;
    if (!formData.role_id) {
      formIsValid = false;
      errors['role_id'] = 'Please select Role name';
    }
    if (!formData.user_id) {
      formIsValid = false;
      errors['user_id'] = 'Please select User';
    }
    setUserrole({
      ...userrole, 
      errors: errors,
    });

    return formIsValid;
  };
  return (
    <div className="container-fluid">
      <div className="title-bar">
          <div className="page-title">
              <h2>Project Category Management</h2>
          </div>
      </div>
      <div className="row">
          <div className="col-md-5">
          <form onSubmit={e => onSubmit(e)} className="page-form">
              <div className="form-group">
                <label> Role Name</label>
                <select name="role_id" className="form-control mt-3" onChange={e => onInputChange(e)}>
                {!isAddMode? '': <option value="">=== Select ===</option>}
                    {roledropdwon.map((row, index) => (
                      <option value={row.role_id} selected={row.role_id == role_id}>{row.role_label}</option>
                    ))}
                </select>
                </div>
                {userrole.errors &&<div className='errorMsg'>{userrole.errors.role_id}</div>}
                <div className="form-group">
                  <label> User Name</label>
                    <select  name="user_id" className="form-control mt-3" onChange={e => onInputChange(e)}>
                    {!isAddMode? '': <option value="">=== Select ===</option>}
                        {userdropdwon.map((row, index) => (
                            <option value={row.user_id} selected={row.user_id == user_id}>{row.user_name}</option>
                        ))}
                    </select>
                </div>
                {userrole.errors &&<div className='errorMsg'>{userrole.errors.user_id}</div>}
                <button className="btn btn-warning btn-block">{isAddMode ? 'Save' : 'Update'} </button>
              </form>
          </div>
      </div>
    </div>
  );
};

export default AddEdituserrole;