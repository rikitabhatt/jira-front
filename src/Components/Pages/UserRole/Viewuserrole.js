import React, { useState, useEffect } from "react";
import axios from "axios";
import { useHistory, useParams , Link } from "react-router-dom";
import { getAllRole, getAllUser } from '../../Helper/Helper.js'; 
const Viewuserrole= () => {
  let history = useHistory();
  const { id } = useParams();
  const [userrole, setUserrole] = useState({
    user_id: "",
    role_id: "",
    create_at: 1,
    
  });
  const { user_id, role_id, create_at } = userrole;
  const [roledropdwon, setRoledropdwon] = useState([]);
  const [userdropdwon, setUserdropdown] = useState([]);

  useEffect(() => {
    loadFromData();
  }, []);
  const loadFromData = async () => {
    const Roledp = getAllRole();  
    setRoledropdwon((await Roledp).data.data);
    const Userdp = getAllUser();
    setUserdropdown((await Userdp).data.data);
    const result = await axios.get(`http://localhost:8000/api/user_role/${id}`);
    setUserrole(result.data.data);
  };
  return (
    <div className="container-fluid">
      <div className="title-bar">
          <div className="page-title">
              <h2>Role Permission Management</h2>
          </div>
      </div>
      <div className="row">
      <div className="col-md-5">
      <form className="page-form">
              <div className="form-group">
                <label> Role Name</label>
                <select name="role_id" className="form-control mt-3" disabled>
                    {roledropdwon.map((row, index) => (
                      <option value={row.role_id} selected={row.role_id == role_id}>{row.role_label}</option>
                    ))}
                </select>
                </div>
                {userrole.errors &&<div className='errorMsg'>{userrole.errors.role_id}</div>}
                <div className="form-group">
                  <label> User Name</label>
                    <select  name="user_id" className="form-control mt-3" disabled>
                        {userdropdwon.map((row, index) => (
                            <option value={row.user_id} selected={row.user_id == user_id}>{row.user_name}</option>
                        ))}
                    </select>
                </div>
                {userrole.errors &&<div className='errorMsg'>{userrole.errors.user_id}</div>}
                <Link className="btn btn-warning btn-block" to={"/userrole"}>Back </Link>
              </form>
          </div>
      </div>
    </div>
  );
};

export default Viewuserrole;