import React, { useState, useEffect } from "react";
import axios from "axios";
import { useHistory, useParams , Link } from "react-router-dom";
import { getAllModules, getAllModuleActions } from '../../Helper/Helper.js'; //callling helper function
const Viewpermission= () => {
  let history = useHistory();
  const { id } = useParams();
  const [permission, setPermission] = useState({
    module_id: "",
    module_action_id: "",
    title: "",
  });
  const [moduleactiondropdwon, setModuleactiondropdwon] = useState([]);
  const [moduledropdwon, setModuledropdwon] = useState([]);
  const {module_id, module_action_id, title } = permission;
  const onInputChange = e => {
    setPermission({ ...permission, [e.target.name]: e.target.value });
  };

  useEffect(() => {
    loadFromData();
  }, []);
  const loadFromData = async () => {
    const Moduledp = getAllModules();  
    setModuledropdwon((await Moduledp).data.data);
    console.log(((await Moduledp).data.data));
    const ActionModuledp = getAllModuleActions();
    setModuleactiondropdwon((await ActionModuledp).data.data);
    const result = await axios.get(`http://localhost:8000/api/permission/${id}`);
    setPermission(result.data.data);
  };
  return (
    <div className="container-fluid">
      <div className="title-bar">
          <div className="page-title">
              <h2>Permission Management</h2>
          </div>
      </div>
      <div className="row">
          <div className="col-md-5">
              <form>
              <div className="form-group">
                <label> Module Name</label>
                <select name="module_id" className="form-control mt-3" disabled>
                    {moduledropdwon.map((row, index) => (
  
                      <option value={row.module_id} selected={row.module_id == module_id}>{row.module_name}</option>
                    ))}
                </select>
                </div>
                <div className="form-group">
                  <label>Module Action Name</label>
                <select  name="module_action_id" className="form-control mt-3" disabled>
                    {moduleactiondropdwon.map((row, index) => (
  
                      <option value={row.action_id} selected={row.action_id == module_action_id}>{row.action_name}</option>
                    ))}
                </select>
                </div>
                <div className="form-group">
                  <input type="text" className="form-control form-control-lg" readOnly name="title" value={title}/>
                </div>
              
                <Link className="btn btn-warning btn-block" to={"/permission"}>Back </Link>
              </form>
          </div>
      </div>
    </div>
  );
};

export default Viewpermission;