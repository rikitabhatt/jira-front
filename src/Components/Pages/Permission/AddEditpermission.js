import React, { useState, useEffect } from "react";
import axios from "axios";
import { useHistory, useParams } from "react-router-dom";
import { getAllModules, getAllModuleActions } from '../../Helper/Helper.js'; //callling helper function
import {  alertService } from '../../../_services';
const AddEditpermission = () => {
  let history = useHistory();
  const { id } = useParams();
  const [permission, setPermission] = useState({
    module_id: "",
    module_action_id: "",
    create_at: 1,
    title:"",
    errors: {},
  });
  const isAddMode = !id;
  const [moduleactiondropdwon, setModuleactiondropdwon] = useState([]);
  const [moduledropdwon, setModuledropdwon] = useState([]);
  const { module_id, module_action_id, title, create_at } = permission;
  const onInputChange = e => {
    setPermission({ ...permission, [e.target.name]: e.target.value });
  };


  const onSubmit = async e => {
    e.preventDefault();
    if(validateForm(permission) == false)
    {
      e.preventDefault();
      return false;
    }  
    let data = {};
    let msg ='';
    if(!isAddMode){ 
      //edit
     data= await axios.put(`http://localhost:8000/api/permission/${id}`, permission);
     msg = 'Edit Record successfully';
    }else {
      // create mode 
      data= await axios.post(`http://localhost:8000/api/permission/`, permission);
      msg = 'Add Record successfully';
    }
    if(data.data.error)
    {
      setPermission({
        ...permission, 
        errors: data.data.error,
      });
      return false;
    }else{
      alertService.success(msg, { keepAfterRouteChange: true });
    }
    history.push("/permission");
  };
  const loadFromData = async () => {
    const Moduledp = getAllModules();  
    setModuledropdwon((await Moduledp).data.data);
    //console.log(((await Moduledp).data.data));
    const ActionModuledp = getAllModuleActions();
    setModuleactiondropdwon((await ActionModuledp).data.data);
    if (!isAddMode) { //edit 
      const result = await axios.get(`http://localhost:8000/api/permission/${id}`);
      setPermission(result.data.data);
    } 
  };
  // Validation function
  const validateForm = (formData) => {
    console.log(formData);
    let errors = {};
    let formIsValid = true;
    if (!formData.module_id) {
      formIsValid = false;
      errors['module_id'] = 'Please select module';
    }
    if (!formData.module_action_id) {
      formIsValid = false;
      errors['module_action_id'] = 'Please select module action';
    }
    if (!formData.title) {
      formIsValid = false;
      errors['title'] = 'Please enter permission titile';
    }
    setPermission({
      ...permission, 
      errors: errors,
    });
    return formIsValid;
  };
  return (
    <div className="container-fluid">
      <div className="title-bar">
          <div className="page-title">
              <h2>Permission Management</h2>
          </div>
      </div>
      <div className="row">
          <div className="col-md-12">
          <form onSubmit={e => onSubmit(e)}>
              <div className="form-group">
              <label className="form-label" >Module<span className="astric text-orange">*</span></label>
                <select name="module_id" className="form-control mt-3" onChange={e => onInputChange(e)}>
                    {moduledropdwon.map((row, index) => (
                      <option value={row.module_id} selected={row.module_id == module_id}>{row.module_name}</option>
                    ))}
                </select>
                </div>
                {permission.errors &&<div className='errorMsg'>{permission.errors.module_id}</div>}
                <div className="form-group">
                <label className="form-label" >Module Action Name<span className="astric text-orange">*</span></label>
                    <select  name="module_action_id" className="form-control mt-3" onChange={e => onInputChange(e)}>
                        {moduleactiondropdwon.map((row, index) => (
                            <option value={row.action_id} selected={row.action_id == module_action_id}>{row.action_name}</option>
                        ))}
                    </select>
                </div>
                {permission.errors &&<div className='errorMsg'>{permission.errors.module_action_id}</div>}
                <div className="form-group">
                <label> Title</label>
                  <input type="text" className="form-control form-control-lg" onChange={e => onInputChange(e)} name="title" value={title}/>
                </div>
                {permission.errors &&<div className='errorMsg'>{permission.errors.title}</div>}
                <button className="btn btn-warning btn-block">{isAddMode ? 'Save' : 'Update'} </button>
              </form>
          </div>
      </div>
    </div>
  );
};

export default AddEditpermission;