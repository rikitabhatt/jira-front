import React, { Component,useState, useEffect } from 'react'
import PropTypes from 'prop-types';
import { Link } from "react-router-dom";
import axios from 'axios';

import { Redirect } from 'react-router';
import { useHistory, useParams } from "react-router-dom";
import CreateTaskModal from './Modal/CreateTaskModal';

export default class Header extends Component {
   
   render() { 
    let popupType = 'create';
    let userToken =  localStorage.getItem('logintoken'); 
    let isLogin = false;
    if(userToken != null) {
        isLogin = true;
      }
        return (
            <div className="header-wrapper fixed-wrapper">
                <div className="container">
                    <header>
                        <nav className="navbar navbar-expand-lg navbar-light bg-light">
                            <Link className="navbar-brand" to={"/"}>
                                <img src="/images/logo.png" alt="logo" className="logo-img"/>
                            </Link>
                             <button className="navbar-toggler" type="button" data-toggle="collapse"
                                data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                                aria-label="Toggle navigation">
                                <span className="navbar-toggler-icon"></span>
                            </button>
                
                            <div className="collapse navbar-collapse" id="navbarSupportedContent">
                                <ul className="navbar-nav mr-auto">
                                <li className="nav-item">
                                        <Link className="nav-link" to={"/company"}>Company</Link>
                                    </li>
                                    <li className="nav-item dropdown">
                                        <Link className="nav-link dropdown-toggle" to={"/dashboard"} id="navbarDropdown" role="button"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Dashboards
                                        </Link>
                                        <div className="dropdown-menu dashboard-dropdown" aria-labelledby="navbarDropdown">
                                            <a className="dropdown-item" href="dashboard.php">View System Dashboard</a>
                                            <a className="dropdown-item" href="#">Manage Dashboards</a>
                                        </div>
                                    </li>
                                    <li className="nav-item dropdown">
                                        <Link className="nav-link dropdown-toggle" to={"/projectmanagment"} id="navbarDropdown" role="button"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Projects
                                        </Link>
                                        <div className="dropdown-menu current-project-dropdown" aria-labelledby="navbarDropdown">
                                            <span className="dropdown-label">Current Projects</span>
                                            <a className="dropdown-item" href="#">
                                                <span className="ti-flag-alt d-task-icon bg-dark-green"></span> I Go Wok (IG)</a>
                                            <a className="dropdown-item" href="#"> <span
                                                    className="ti-flag-alt d-task-icon bg-dark-green"></span> Consumer Sketch
                                                (CS)</a>
                                            <a className="dropdown-item" href="#"> <span
                                                    className="ti-flag-alt d-task-icon bg-dark-green"></span> Hunter Express
                                                (HUN)</a>
                                            <div className="dropdown-divider"></div>
                                            <span className="dropdown-label">Recent Projects</span>
                                            <a className="dropdown-item" href="#"><span
                                                    className="ti-flag-alt d-task-icon bg-dark-green"></span> I Go Wok (IG)</a>
                                            <a className="dropdown-item" href="#"><span
                                                    className="ti-flag-alt d-task-icon bg-dark-green"></span> Consumer Sketch
                                                (CS)</a>
                                            <a className="dropdown-item" href="#"><span
                                                    className="ti-flag-alt d-task-icon bg-dark-green"></span> Hunter Express
                                                (HUN)</a>
                                            <a className="dropdown-item" href="#"><span
                                                    className="ti-flag-alt d-task-icon bg-dark-green"></span> Dealzee (DE)</a>
                                            <div className="dropdown-divider"></div>
                                            <a className="dropdown-item" href="#">All Projects</a>
                                        </div>
                                    </li>
                                    <li className="nav-item dropdown">
                                        <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            issues
                                        </a>
                                        <div className="dropdown-menu issue-dropdown" aria-labelledby="navbarDropdown">
                                            <span className="dropdown-label">Current issues</span>
                                            <a className="dropdown-item" href="#"><span
                                                    className="ti-bookmark d-task-icon bg-light-green"></span> IG-61 Employer (Email
                                                Candidate..</a>
                                            <a className="dropdown-item" href="#"><span
                                                    className="ti-control-record d-task-icon bg-orange"></span> CS -20 new web
                                                design issues..</a>
                                            <a className="dropdown-item" href="#"><span className="ti-check d-task-icon bg-blue"></span>
                                                HUN 34 client side pending tas..</a>
                                            <div className="dropdown-divider"></div>
                                            <a className="dropdown-item" href="#">my open issues</a>
                                            <a className="dropdown-item" href="#">reported by me</a>
                                        </div>
                                    </li>
                                    <li className="nav-item dropdown">
                                        <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Boards
                                        </a>
                                        <div className="dropdown-menu boards-dropdown" aria-labelledby="navbarDropdown">
                                            <a className="dropdown-item" href="#">IG Board</a>
                                            <a className="dropdown-item" href="#">CS Board</a>
                                            <a className="dropdown-item" href="#">HUN board</a>
                                            <a className="dropdown-item" href="#">IGoWok (IG)</a>
                                            <a className="dropdown-item" href="#">KNC Board</a>
                                            <a className="dropdown-item" href="#">Hoya Board</a>
                                            <a className="dropdown-item" href="#">DE Board</a>
                                            <div className="dropdown-divider"></div>
                                            <a className="dropdown-item" href="projects.php">All Projects</a>
                                        </div>
                                    </li>
                                    <li className="nav-item">
                                        <CreateTaskModal className="nav-link" Component={this.sync} popupType={popupType}></CreateTaskModal>
                                    </li>
                                    {isLogin === true ?
                                    <li className="nav-item mr-auto">
                                        <Link className="nav-link" to={"/logout"}> Logout </Link>
                                    </li> : ''}
                                </ul>
                                {isLogin === true ?<>
                                <form className=" form-inline top-search-bar">
                                    <input className="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search"/>
                                </form> 
                                <div className="arrow-none notification-dropdown dropdown">
                                    <a className="nav-link dropdown-toggle" href="#" id="notificationDropdown" role="button"
                                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <span className="ti-bell notification-icon"><b>20</b></span>
                                    </a>
                                    <div className="dropdown-menu notification-drop" aria-labelledby="notificationrDropdown">
                                        <a className="dropdown-item" href="#">
                                            <div className="notification-block">
                                                Assigned IG-63 - Employer (Search Candidate) to you
                                            </div>
                                        </a>
                                        <a className="dropdown-item" href="#">
                                            <div className="notification-block">
                                                Assigned IG-63 - Employer (Search Candidate) to you
                                            </div>
                                        </a>
                                        <a className="dropdown-item" href="#">
                                            <div className="notification-block">
                                                Assigned IG-63 - Employer (Search Candidate) to you
                                            </div>
                                        </a>
                                        <a className="dropdown-item" href="#">
                                            <div className="notification-block">
                                                Assigned IG-63 - Employer (Search Candidate) to you
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                
                                <div className="user-dropdown arrow-none dropdown">
                                    <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <img src="images/useravatar.png" alt="avatar" className="avatar-img"/>
                                    </a>
                                    <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                                        <a className="dropdown-item" href="viewprofile.php">Profile</a>
                                        <a className="dropdown-item" href="dashboard.php">Dashboard</a>
                                        <a className="dropdown-item" href="#">Boards</a>
                                        <a className="dropdown-item" href="task.php">Issues</a>
                                        <a className="dropdown-item" href="#">Logout</a>
                                    </div>
                                </div></> : ''}
                            </div>
                        </nav>
                    </header>
                </div>
            </div>
        )
    }
}
