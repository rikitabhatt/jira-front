import React, { useState, useEffect } from "react";
import axios from "axios";  
const getAllModules=()=>{
    console.log('Hello..!');
    const res = axios.post("http://localhost:8000/api/module/getAllRecord");
    return res;
}
 const getAllModuleActions=()=>{
    const res = axios.post("http://localhost:8000/api/module_action/getAllRecord");
    return res;
}
 const getAllPermission=()=>{
    const res = axios.post("http://localhost:8000/api/permission/getAllRecord");
    return res;
}
const getAllRole=()=>{
    const res = axios.post("http://localhost:8000/api/roles/getAllRecord");
    return res;
}
const getAllUser=()=>{
    const res = axios.post("http://localhost:8000/api/user/getAllRecord");
    return res;
}
const getAllUserbyrole=(role_id)=>{
    const res = axios.post(`http://localhost:8000/api/user/getAllRecord/${role_id}`);
    return res;
}
const getAllProjectCategory=()=>{
    const res = axios.post("http://localhost:8000/api/project_category/getAllRecord");
    return res;
}
const getAllDesignation=()=>{
    const res = axios.post("http://localhost:8000/api/designation/getAllRecord");
    return res;  
}
const getAlltaskType=()=>{
    const res = axios.get("http://localhost:8000/api/tasks/getAlltaskType");
    return res;  
}
const getAllProject=()=>{
   
    const res = axios.post("http://localhost:8000/api/project/getAllRecord");
    return res;  
}

const getAllAsssigneeProject=( project_id="")=>{
    let res;
    if(project_id !=""){
         res = axios.post("http://localhost:8000/api/project_user_assign/getAllRecord", {
            project_id: project_id,
        });
    }else{
        res = axios.post("http://localhost:8000/api/user/getAllRecord");
       
    }
   
    return res;
      
}

export { getAllModules,getAllModuleActions,getAllPermission,getAllRole,getAllUser,getAllProjectCategory,getAllDesignation,
    getAlltaskType,getAllProject, getAllAsssigneeProject, getAllUserbyrole }