import React from "react";
import { BrowserRouter as Router , Switch , Route } from "react-router-dom";
import  Fullpage  from "../Layouts/Fullpage";
import  Chillertheme  from "../Layouts/Chillertheme";
import LoginLayout  from "../Layouts/LoginLayout";
import { Redirect } from 'react-router';

import { Dashboard } from "../Pages/Dashboard";
import  Login  from "../Pages/Login";
import  Logout  from "../Pages/Logout";
import { Company } from "../Pages/Company/Company";
import AddEditcompany  from "../Pages/Company/AddEditcompany";
import Viewcompany from "../Pages/Company/Viewcompany";

import { User } from "../Pages/User/User";
import AddEdituser  from "../Pages/User/AddEdituser";
import Viewuser from "../Pages/User/Viewuser";

import { Role } from "../Pages/Role/Role";
import AddEditrole  from "../Pages/Role/AddEditrole";
import Viewrole from "../Pages/Role/Viewrole"

import { Module } from "../Pages/Module/Module";
import AddEditmodule  from "../Pages/Module/AddEditmodule";
import Viewmodule from "../Pages/Module/Viewmodule"

import { Action } from "../Pages/ModuleAction/Action";
import AddEditaction  from "../Pages/ModuleAction/AddEditaction";
import Viewaction from "../Pages/ModuleAction/Viewaction";

import { Permission } from "../Pages/Permission/Permission";
import AddEditpermission    from "../Pages/Permission/AddEditpermission";
import Viewpermission  from "../Pages/Permission/Viewpermission";

import { Rolepermission } from "../Pages/RolePermission/Rolepermission";
import AddEditrolepermission  from "../Pages/RolePermission/AddEditrolepermission";
import Viewrolepermission from "../Pages/RolePermission/Viewrolepermission";

import { Userrole } from "../Pages/UserRole/Userrole";
import AddEdituserrole  from "../Pages/UserRole/AddEdituserrole";
import Viewuserrole from "../Pages/UserRole/Viewuserrole";

import { Projectcategory } from "../Pages/ProjectCategory/Projectcategory";
import AddEditprojectcategory  from "../Pages/ProjectCategory/AddEditprojectcategory";
import Viewprojectcategory from "../Pages/ProjectCategory/Viewprojectcategory";

import { Project } from "../Pages/Project/Project";
import AddEditproject  from "../Pages/Project/AddEditproject";
import Viewproject from "../Pages/Project/Viewproject";
import { Settingproject } from "../Pages/Project/Settingproject";

import { Task } from "../Pages/Task/Task";
import { TaskDetails } from "../Pages/Task/TaskDetails";
export default function index() {
  const userToken =  localStorage.getItem('logintoken');
  let isLogin = false;
  if(userToken != null) {
    isLogin = true;
  }
  console.log(isLogin);
  const LoginpageRoute =({ component: Component, ...rest }) => {
   
    return (
     
      <Route
        {...rest}
        render={matchProps => (
          !isLogin ?
         <LoginLayout>
            <Component {...matchProps} />
          </LoginLayout>  :
            <Redirect to='/dashboard' />
        )}
      />
    );
  };
  const FullpageRoute = ({ component: Component, ...rest }) => {
    return (
      <Route
        {...rest}
        render={matchProps => (
          isLogin ?
          <Fullpage>
            <Component {...matchProps} />
          </Fullpage> :
            <Redirect to='/login' />
        )}
      />
    );
  };
  const ChillerthemeLayoutRoute = ({ component: Component, ...rest }) => {
    return (
      <Route
        {...rest}
        render={matchProps => (
          isLogin ?
          <Chillertheme>
            <Component {...matchProps} />
          </Chillertheme>:
          <Redirect to='/login' />
        )}
      />
    );
  };
  return (
    <>
    
      <Switch>
         
        <Route exact path="/logout" component={Logout}/>
        <LoginpageRoute exact path="/login" component={Login}/>
        <FullpageRoute exact path="/" component={Dashboard}/> 
        <FullpageRoute exact path="/dashboard" component={Dashboard}/>
        
        <ChillerthemeLayoutRoute exact path="/company" component={Company}/>
        <ChillerthemeLayoutRoute exact path="/company/edit/:id" component={AddEditcompany}/>
        <ChillerthemeLayoutRoute exact path="/company/view/:id" component={Viewcompany}/>
        <ChillerthemeLayoutRoute exact path="/company/add" component={AddEditcompany}/>

        <ChillerthemeLayoutRoute exact path="/user" component={User}/>
        <ChillerthemeLayoutRoute exact path="/user/edit/:id" component={AddEdituser}/>
        <ChillerthemeLayoutRoute exact path="/user/view/:id" component={Viewuser}/>
        <ChillerthemeLayoutRoute exact path="/user/add" component={AddEdituser}/>

        <ChillerthemeLayoutRoute exact path="/role" component={Role}/>
        <ChillerthemeLayoutRoute exact path="/role/edit/:id" component={AddEditrole}/>
        <ChillerthemeLayoutRoute exact path="/role/view/:id" component={Viewrole}/>
        <ChillerthemeLayoutRoute exact path="/role/add" component={AddEditrole}/>

        <ChillerthemeLayoutRoute exact path="/module" component={Module}/>
        <ChillerthemeLayoutRoute exact path="/module/edit/:id" component={AddEditmodule}/>
        <ChillerthemeLayoutRoute exact path="/module/view/:id" component={Viewmodule}/>
        <ChillerthemeLayoutRoute exact path="/module/add" component={AddEditmodule}/>

        <ChillerthemeLayoutRoute exact path="/moduleaction" component={Action}/>
        <ChillerthemeLayoutRoute exact path="/moduleaction/edit/:id" component={AddEditaction}/>
        <ChillerthemeLayoutRoute exact path="/moduleaction/view/:id" component={Viewaction}/>
        <ChillerthemeLayoutRoute exact path="/moduleaction/add" component={AddEditaction}/>

        <ChillerthemeLayoutRoute exact path="/permission" component={Permission}/>
        <ChillerthemeLayoutRoute exact path="/permission/edit/:id" component={AddEditpermission}/>
        <ChillerthemeLayoutRoute exact path="/permission/view/:id" component={Viewpermission}/>
        <ChillerthemeLayoutRoute exact path="/permission/add" component={AddEditpermission}/>

        <ChillerthemeLayoutRoute exact path="/permission" component={Permission}/>
        <ChillerthemeLayoutRoute exact path="/permission/edit/:id" component={AddEditpermission}/>
        <ChillerthemeLayoutRoute exact path="/permission/view/:id" component={Viewpermission}/>
        <ChillerthemeLayoutRoute exact path="/permission/add" component={AddEditpermission}/>

        <ChillerthemeLayoutRoute exact path="/rolepermission" component={Rolepermission}/>
        <ChillerthemeLayoutRoute exact path="/rolepermission/edit/:id" component={AddEditrolepermission}/>
        <ChillerthemeLayoutRoute exact path="/rolepermission/view/:id" component={Viewrolepermission}/>
        <ChillerthemeLayoutRoute exact path="/rolepermission/add" component={AddEditrolepermission}/>
        
        <ChillerthemeLayoutRoute exact path="/userrole" component={Userrole}/>
        <ChillerthemeLayoutRoute exact path="/userrole/edit/:id" component={AddEdituserrole}/>
        <ChillerthemeLayoutRoute exact path="/userrole/view/:id" component={Viewuserrole}/>
        <ChillerthemeLayoutRoute exact path="/userrole/add" component={AddEdituserrole}/>

        <ChillerthemeLayoutRoute exact path="/projectcategory" component={Projectcategory}/>
        <ChillerthemeLayoutRoute exact path="/projectcategory/edit/:id" component={AddEditprojectcategory}/>
        <ChillerthemeLayoutRoute exact path="/projectcategory/view/:id" component={Viewprojectcategory}/>
        <ChillerthemeLayoutRoute exact path="/projectcategory/add" component={AddEditprojectcategory}/>

        <ChillerthemeLayoutRoute exact path="/project" component={Project}/>
        <ChillerthemeLayoutRoute exact path="/project/edit/:id" component={AddEditproject}/>
        <ChillerthemeLayoutRoute exact path="/project/view/:id" component={Viewproject}/>
        <ChillerthemeLayoutRoute exact path="/project/add" component={AddEditproject}/>
        <ChillerthemeLayoutRoute exact path="/project/setting/:id" component={Settingproject}/>
        
        <ChillerthemeLayoutRoute exact path="/task" component={Task}/>
        <ChillerthemeLayoutRoute exact path="/task_details/:taskSlug" component={TaskDetails}/>
      </Switch>
      
    </>
  );
}
