import React, { useState, useEffect } from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import {Button,Modal} from "react-bootstrap";
import { useHistory, useParams, useLocation } from "react-router-dom";
import { CKEditor } from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-editor-classic/src/classiceditor';
import {  alertService } from '../../_services';
import axios from "axios";
function EditCommentModal(props) {
    let commentDetails = props.CommentDetails;
    const handleToUpdate = props.handleToUpdate;
    let history = useHistory();
    const location = useLocation();
    const [show, setShow] = useState(false);
    const handleShow = () => { setShow(true)
        setEditdata({ ...editdata, 
            comment: props.CommentDetails.comment,
            task_id :  props.CommentDetails.task_id,
            created_by:props.CommentDetails.create_by,
            id: props.CommentDetails.id,
        });
    };
    const handleClose = () => setShow(false);
    const [editdata, setEditdata] = useState({
        id: commentDetails.id,
        task_id: commentDetails.task_id ,
        comment: commentDetails.comment,
        created_by:commentDetails.create_by,
        errors:{},
    });
    const { id,task_id, comment, created_by } = editdata;
    const onInputChange = async (e) => {
        setEditdata({ ...editdata, [e.target.name]: e.target.value });
    };
   
    const onSubmit = async e => {
        e.preventDefault();
        if(validateForm(editdata) == false)
        {
          e.preventDefault();
          return false;
        }  
        let data = {};
        let msg ='';
       
        data= await axios.put(`http://localhost:8000/api/task_comment/${id}`, editdata);
        msg = 'Comments edit Successfully';
        handleToUpdate(commentDetails.task_id);
        if(data.data.error)
        {
            setEditdata({
            ...editdata, 
            errors: data.data.error,
        });
        }else{  
            setShow(false);
            alertService.success(msg, { keepAfterRouteChange: true });
            
        }
        //console.log(location.pathname); 
        //history.push(location.pathname);
       // window.location.reload(false);

    };
    const validateForm = (formData) => {
        let errors = {};
        let formIsValid = true;

        if (!formData.comment) {
          formIsValid = false;
          errors['comment'] = 'Please enter comment';
        }
        setEditdata({
          ...editdata, 
          errors: errors,
        });
        return formIsValid;
    };
    
    return (
        <> 
        <a onClick={handleShow}  class="comments-icon-list" data-toggle="modal" data-target="#edit-comment-popup"><span class="ti-pencil"></span></a>
        <Modal show={show} onHide={handleClose}  size="lg">
        <Modal.Header>
          <Modal.Title><h5 className="modal-title" id="exampleModalLabel"> Edit Comment</h5></Modal.Title>
        </Modal.Header>
         <Modal.Body>
            <form>
            <div class="form-height">
                
                            <div class="form-group m-b-5">
                                <div class="row align-items-center">
                                    <div class="col-md-3 p-r-0">
                                        <label for="acc-role">Author</label>
                                    </div>
                                    <div class="col-md-7">
                                        <label class="value-label text-left"><b>{commentDetails.createby_details.name}</b></label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row align-items-center">
                                    <div class="col-md-3 p-r-0">
                                        <label for="acc-role">Created</label>
                                    </div>
                                    <div class="col-md-7">
                                        <label class="value-label text-left"><b>{commentDetails.comment_time}</b></label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row align-items-center">
                                    <div class="col-md-3 p-r-0">
                                        <label for="acc-phone">Comment</label>
                                    </div>
                                    <div class="col-md-7">
                                        <div class="form-field">
                                            <textarea id="comment" name="comment" rows="10" cols="60" value={comment}  
                                            onChange={e => onInputChange(e)}></textarea>
                                        </div>
                                        {editdata.errors &&<div className='errorMsg'>{editdata.errors.comment}</div>}
                                    </div>
                                </div>
                            </div>

                        </div>
                <div className="modal-footer p-b-0">
                    <Button className="btn btn-white" onClick={handleClose}>Close</Button>
                    <Button className="btn btn-green" disabled={!commentDetails.comment}  variant="primary" onClick={e => onSubmit(e)}>Save Change</Button>
                </div>
            </form>
         </Modal.Body>
        
        </Modal>
        </>
);
  }
export default EditCommentModal;