import React, { useState, useEffect } from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import {Button,Modal} from "react-bootstrap";
import { useHistory, useParams, useLocation } from "react-router-dom";
import { CKEditor } from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-editor-classic/src/classiceditor';
import { getAllProject, getAlltaskType, getAllUser, getAllAsssigneeProject} from '../Helper/Helper'; 
import {  alertService } from '../../_services';
import axios from "axios";
function CreateTaskModal(props) {
    let popupType;
    
    if(props.popupType){
        popupType = props.popupType;
    }else {
        popupType='';
    }
    let history = useHistory();
    const location = useLocation();
    const [show, setShow] = useState(false);
     const handleShow = () => {
        if(popupType !='create'){
            GetData(props.TaskId);
           // alert(props.TaskId);
        }
        setShow(true)
    };
   
    const handleClose = () => setShow(false);
    const [projectropdwon,setProjectropdwon] = useState([]);
    const [tasktypedropdwon,setTasktypedropdwon] = useState([]);
    const [userdropdown, setGetAllUser]  = useState([]);
    const [createtask, setCreatetask] = useState({
        project_id: "",
        issue_type: "",
        issue_title: "",
        description:"",
        priority: "",
        assignee_id :"",
        attachment: "",
        errors:{},
    });
    async function GetData(id){
        if(popupType !='create'){
            const res =  await axios.get(`http://localhost:8000/api/task/${id}`);
            /*setTask({ loading: false, results: res.data.data });*/
            setCreatetask(res.data.data);
            console.log('yee'+res.data.data);
        }
    }
    
    const { project_id,issue_type, issue_title, description, priority, assignee_id  } = createtask;
    const onInputChange = async (e) => {
        if(e.target.name =='project_id'){
            const Userdp = getAllAsssigneeProject(e.target.value);  
            setGetAllUser((await Userdp).data.data);
        }
        setCreatetask({ ...createtask, [e.target.name]: e.target.value });
     };
      useEffect(() => {
        loadFromData();
    }, []);
    const loadFromData = async () => {
        const Projectdp = getAllProject();  
        setProjectropdwon((await Projectdp).data.data);
        const Tasktypedp = getAlltaskType();  
        setTasktypedropdwon((await Tasktypedp).data.data);
        const Userdp = getAllAsssigneeProject(project_id);  
        setGetAllUser((await Userdp).data.data);
    };
    const onSubmit = async e => {
        e.preventDefault();
   
        if(validateForm(createtask) == false)
        {
          e.preventDefault();
          return false;
        }  
        let data = {};
        let msg ='';
        if(props.popupType !='create'){
            let id = props.TaskId;
            data= await axios.put(`http://localhost:8000/api/task/${id}`, createtask);
            msg = 'Edit record successfully';
        }else{
        data= await axios.post(`http://localhost:8000/api/task/`, createtask);
        msg = 'Task created Successfully';
        }
       
        if(data.data.error)
        {
            setCreatetask({
            ...createtask, 
            errors: data.data.error,
        });
        return false;
        }else{  
            setShow(false);
            alertService.success(msg, { keepAfterRouteChange: true });

        }
        if(props.popupType =='edit'){
        //history.push(`/task_details/${props.TaskId}`);
        //console.log(location.pathname); 
        //alert(location.pathname);
        window.location.reload();
        }
      };
      const validateForm = (formData) => {
       
        let errors = {};
        let formIsValid = true;
    
        if (!formData.project_id) {
          formIsValid = false;
          errors['project_id'] = 'Please enter your Company name';
        }
    
        if (!formData.issue_type) {
          formIsValid = false;
          errors['issue_type'] = 'Please select issue type';
        }
    
        if (!formData.issue_title) {
          formIsValid = false;
          errors['issue_title'] = 'Please enter your issue title';
        }
    
        if (!formData.description) {
          formIsValid = false;
          errors['description'] = 'Please enter your Description';
        }
    
        if (!formData.priority) {
          formIsValid = false;
          errors['priority'] = 'Please select priority';
        }
        if (!formData.assignee_id) {
            formIsValid = false;
            errors['assignee_id'] = 'Please select Assignee';
        }
    
        setCreatetask({
          ...createtask, 
          errors: errors,
        });
    
        return formIsValid;
      };
    
      return (
        <>
        { popupType =='create' ?
        <button className="nav-link btn btn-green"  onClick={handleShow} > Create </button>:
        <li>
           <a onClick={handleShow} className="btn btn-sky-blue task-action-btn btn-icon" data-toggle="modal" data-target="#create-issue-modal"><span className="ti-pencil"></span>Edit</a>
        </li>
        }
        <Modal show={show} onHide={handleClose}  size="lg">
        <Modal.Header>
          <Modal.Title><h5 className="modal-title" id="exampleModalLabel"> { popupType =='create' ?'Create':'Edit'} issue</h5></Modal.Title>
        </Modal.Header>
         <Modal.Body>
            <form>
                <div className="form-height">
                    <div className="form-group">
                        <div className="row align-items-center">
                            <div className="col-md-3 p-r-0">
                                <label for="project">Project<span className="astric text-orange">*</span></label>
                            </div>
                            <div className="col-md-7">
                                <div className="form-field">
                                    <select  name="project_id" className="form-control" onChange={e => onInputChange(e)}>
                                    <option value="" > Select Project {project_id} </option>
                                            {projectropdwon.map((row, index) => (
                                                <option value={row.project_id} selected={row.project_id == project_id}>{row.project_name} ({row.project_key})</option>
                                            ))}
                                    </select>
                                </div>
                                {createtask.errors &&<div className='errorMsg'>{createtask.errors.project_id}</div>}
                            </div>
                        </div>
                    </div>

                    <div className="form-group">
                        <div className="row align-items-center">
                            <div className="col-md-3 p-r-0">
                                <label for="project">Issue type<span className="astric text-orange">*</span></label>
                            </div>
                            <div className="col-md-7">
                                <div className="form-field">
                                <select  name="issue_type" className="form-control" onChange={e => onInputChange(e)}>
                                    <option value="" > Select Issue Type </option>
                                    {tasktypedropdwon.map((row, index) => (
                                        <option value={row.type}  selected={row.type == issue_type}>{row.name} </option>
                                    ))}
                                            
                                </select>
                                </div>
                                {createtask.errors &&<div className='errorMsg'>{createtask.errors.issue_type}</div>}
                            </div>
                        </div>
                    </div>

                    <hr></hr>

                    <div className="form-group">
                        <div className="row align-items-center">
                            <div className="col-md-3 p-r-0">
                                <label for="summary">Summary<span className="astric text-orange">*</span></label>
                            </div>
                            <div className="col-md-7">
                                <div className="form-field">
                                    <input type="text" name="issue_title" value={issue_title} className="form-control" id="summary" onChange={e => onInputChange(e)}/>
                                </div>
                            </div>
                            {createtask.errors &&<div className='errorMsg'>{createtask.errors.issue_title}</div>}
                        </div>
                    </div>
                    <div className="form-group">
                        <div className="row align-items-center">
                            <div className="col-md-3 p-r-0">
                                <label for="summary">Description</label>
                            </div>
                            <div className="col-md-7">
                                <div className="form-field">
                                <textarea id="editor1" name="description" rows="5" cols="59"  onChange={e => onInputChange(e)} value={description}> </textarea>
                                </div>
                            </div>
                            {createtask.errors &&<div className='errorMsg'>{createtask.errors.description}</div>}
                        </div>
                    </div>
                    <div className="form-group">
                        <div className="row align-items-center">
                            <div className="col-md-3 p-r-0">
                                <label for="priority">Priority</label>
                            </div>
                            <div className="col-md-7">
                                <div className="form-field">
                                    <select className="form-control" id="priority" name="priority" onChange={e => onInputChange(e)}>
                                        <option value="" > Select Priority Type </option>
                                        <option value="1" selected={priority == 1}>Very High</option>
                                        <option value="2"  selected={priority == 2}>High</option>
                                        <option value="3"  selected={priority == 3}>Medium</option>
                                        <option value="4"  selected={priority == 4}>Low</option>
                                    </select>
                                </div>
                                {createtask.errors &&<div className='errorMsg'>{createtask.errors.priority}</div>}
                            </div>
                        </div>
                    </div>
                    <div className="form-group">
                        <div className="row align-items-center">
                            <div className="col-md-3 p-r-0">
                                <label for="attachment">Attachment :</label>
                            </div>
                            <div className="col-md-7">
                                <div className="form-field">
                                    <input type="file" name="attachment" onChange={e => onInputChange(e)}/>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div className="form-group">
                        <div className="row align-items-center">
                            <div className="col-md-3 p-r-0">
                                <label for="assignee">Assignee</label>
                            </div>
                            <div className="col-md-7">
                                <div className="form-field">
                                    <select  name="assignee_id" className="form-control" onChange={e => onInputChange(e)}>
                                    <option value="" > Select Assignee </option>
                                    {userdropdown.map((row, index) => (
                                        <option value={row.user_id} selected={row.user_id == assignee_id}>{row.user_name} </option>
                                    ))}
                                            
                                    </select>
                                </div>
                                {createtask.errors &&<div className='errorMsg'>{createtask.errors.assignee_id}</div>}
                            </div>
                        </div>
                    </div>
                   {/*<div className="form-group">
                        <div className="row align-items-center">
                            <div className="col-md-3 p-r-0">
                                <label for="epiclink">Epic Link
                                </label>
                            </div>
                            <div className="col-md-7">
                                <div className="form-field">
                                    <select className="form-control" id="epiclink">
                                        <option>Default select</option>
                                        <option>Default select</option>
                                        <option>Default select</option>
                                        <option>Default select</option>
                                        <option>Default select</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="form-group">
                        <div className="row align-items-center">
                            <div className="col-md-3 p-r-0">
                                <label for="sprint">Sprint</label>
                            </div>
                            <div className="col-md-7">
                                <div className="form-field">
                                    <select className="form-control" id="sprint">
                                        <option>May WK4 (Active Sprint)</option>
                                        <option>May WK4</option>
                                        <option>May WK4</option>
                                        <option>May WK4</option>
                                        <option>May WK4</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                                    </div> */}
                </div>
                <div className="modal-footer p-b-0">
                    <Button className="btn btn-secondary" onClick={handleClose}>Close</Button>
                    <Button className="btn btn-primary" variant="primary" onClick={e => onSubmit(e)}>Save Change</Button>
                </div>
            </form>
         </Modal.Body>
        
        </Modal>
    </>

);
  }
export default CreateTaskModal;