import React, { useState, useEffect } from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import {Button,Modal} from "react-bootstrap";
import { useHistory, useParams, useLocation } from "react-router-dom";
import {getAllAsssigneeProject} from '../Helper/Helper'; 
import {  alertService } from '../../_services';
import axios from "axios";
function UpdateTaskAssigneeModal(props) {
    let TaskId = props.TaskId;
    let history = useHistory();
    const location = useLocation();
    const [show, setShow] = useState(false);
    const handleShow = () => { setShow(true) };
    const handleClose = () => setShow(false);
    const [editdata, setEditdata] = useState({  
      assignee_id :props.AssigneeId,
      task_id: props.TaskId,
      old_assignee_id: props.AssigneeId,
      errors:{} });
    const { assignee_id ,old_assignee_id,task_id } = editdata;
    const [asssigneeProject, setAsssigneeProject] = useState([]);
    const onInputChange = async (e) => {
      setEditdata({ ...editdata, [e.target.name]: e.target.value });
    };
    useEffect(() => {
      loadFromData();
    }, []);
    const loadFromData = async () => {
        const AsssigneeProjectdp = getAllAsssigneeProject(props.ProjectId);  
        let res = (await AsssigneeProjectdp).data.data;
        
        setAsssigneeProject(res); 
    };
    const onSubmit = async e => {
         e.preventDefault();
        if(validateForm(editdata) == false)
        {
          e.preventDefault();
          return false;
        }  
        let data = {};
        let msg ='';
        data= await axios.post (`http://localhost:8000/api/updateTaskAssignee/`,editdata);
        msg = 'Assignee Update Successfully';
        if(data.data.error)
        {
            setEditdata({
            ...editdata, 
            errors: data.data.error,
        });
        }else{  
            setShow(false);
            alertService.success(msg, { keepAfterRouteChange: true });
        }
        //console.log(location.pathname); 
        //history.push(location.pathname);
        //window.location.reload();
        this.forceUpdate();
    };
    const validateForm = (formData) => {
        let errors = {};
        let formIsValid = true;

        if (!formData.assignee_id) {
          formIsValid = false;
          errors['assignee_id'] = 'Select assignee';
        }
        setEditdata({
          ...editdata, 
          errors: errors,
        });
        return formIsValid;
      };
      return (
        <>
         <li>
            <a onClick={handleShow} className="btn btn-sky-blue task-action-btn btn-icon" data-toggle="modal" data-target="#assignee-popup"><span className=" ti-user"></span>Assign</a>
        </li>
        <Modal show={show} onHide={handleClose} >
        <Modal.Header>
          <Modal.Title><h5 className="modal-title" id="exampleModalLabel">Edit Assignee</h5></Modal.Title>
        </Modal.Header>
         <Modal.Body>
           <form>
                <div className="form-height">
                    <div className="form-group">
                        <div className="row align-items-center">
                            <div className="col-md-3 p-r-0">
                                <label htmlFor="acc-role">Assignee<span className="astric text-orange">*</span></label>
                            </div>
                            <div className="col-md-7">
                                <div className="form-field">
                                  <input type="hidden" name="task_id" value={task_id}/>
                                    <select className="form-control" name="assignee_id" onChange={e => onInputChange(e)}>
                                      <option>select Assignee</option>
                                      {asssigneeProject.map((row, index) => (
                                        <option value={row.user_id} selected={row.user_id == assignee_id}>{row.user_name}</option>
                                      ))}
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="modal-footer p-b-0">
                    <Button className="btn btn-white" onClick={handleClose}>Close</Button>
                    <Button className="btn btn-green" variant="primary" onClick={e => onSubmit(e)}>Save Change</Button>
                </div>
            </form>
         </Modal.Body>
        
        </Modal>
        </>
);
  }
export default UpdateTaskAssigneeModal;