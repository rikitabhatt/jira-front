import React, { useState, useEffect } from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import {Button,Modal} from "react-bootstrap";
import { useHistory, useParams } from "react-router-dom";
import { getAllDesignation, getAllUser} from '../Helper/Helper'; 

import axios from "axios";
function AssginMemberModal() {
    let history = useHistory();
    const { id } = useParams();
    const [show, setShow] = useState(false);
    const handleShow = () => setShow(true);
    const handleClose = () => setShow(false);
    const [designationdropdwon, setDesignationdropdwon] = useState([]);
    const [userdropdwon, setUserdropdwon] = useState([]);
    const [assignmember, setAssignmember] = useState({
        designation_id: "",
        user_id: "",
        create_by: 1,
        project_id:id
      });
      const { project_id,designation_id, user_id, create_by } = setAssignmember;
      const onInputChange = e => {
        setAssignmember({ ...assignmember, [e.target.name]: e.target.value });
      };
      useEffect(() => {
        loadFromData();
      }, []);
      const loadFromData = async () => {
        const Designationdp = getAllDesignation();  
        setDesignationdropdwon((await Designationdp).data.data);
        const Useredp = getAllUser();  
        setUserdropdwon((await Useredp).data.data);
      };
      const onSubmit = async e => {
        e.preventDefault();
        console.log(id);
        const data= await axios.post(`http://localhost:8000/api/project_user_assign/`, assignmember);
        setShow(false);
        history.push(`/project/setting/${id}`);
        window.location.reload(false);
      };
    return (
        
      <div>
    
        <Button className="btn btn-success"  onClick={handleShow} style={{borderRadius: '10px'}}>   Add Member </Button> 
        <Modal show={show} onHide={handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>Add Project Member</Modal.Title>
          </Modal.Header>
        
           <Modal.Body>
            <form>
            <div className="form-height">
                <div className="form-group">
                    <div className="row align-items-center">
                        <div className="col-md-3 p-r-0">
                            <label for="project">Member<span className="astric text-orange">*</span></label>
                        </div>
                        <input type="hidden" value={project_id} name="project_id"/>
                        <div className="col-md-7">
                            <div className="form-field">
                              <select  name="user_id" className="form-control" onChange={e => onInputChange(e)}>
                              <option value="" >== Select User --</option>
                                    {userdropdwon.map((row, index) => (
                                        <option value={row.user_id} >{row.user_name}</option>
                                    ))}
                              </select>
                            </div>
                               
                            </div>
                        </div>
                    </div>
                </div>
                <div className="form-group">
                    <div className="row align-items-center">
                        <div className="col-md-3 p-r-0">
                            <label>Designation<span className="astric text-orange">*</span></label>
                        </div>
                        <div className="col-md-7">
                            <div className="form-field">
                            <select  name="designation_id" className="form-control" onChange={e => onInputChange(e)}>
                            <option value="" >== Select Designation --</option>
                                    {designationdropdwon.map((row, index) => (
                                        <option value={row.id} >{row.designation_name}</option>
                                    ))}
                                </select>
                           
                        </div>
                    </div>
                </div>
                <div className="modal-footer">
                    <Button className="btn btn-secondary" onClick={handleClose}>Close</Button>
                    <Button className="btn btn-primary" variant="primary" onClick={e => onSubmit(e)}>Add</Button>
                </div>
                
          
            </div>
            </form>
          </Modal.Body>
        
        </Modal>
      </div>
    );
  }
export default AssginMemberModal;