//rafc
import React from 'react'
import {  Alert } from '../../_component';
import { Sidebar} from "../Sidebar";
 const Chillertheme = ({ children }) => {
    return (
        <div className="page-wrapper chiller-theme">
            <Sidebar/>
            <main className="page-content">
                <Alert/>
                {children}
            </main>
        </div>
    );
};
export default Chillertheme;
