import React from 'react'
import {  Alert } from '../../_component';
 const Fullpage = ({ children }) => {
    return (
        <div className="page-wrapper">
            <Alert/>
            <div className="dashboard-wrapper simple-page-wrapper p-t-10">
                <div className="container">
                    <h3>System Dashboard</h3>
                     {children}
                </div>
            </div>
        </div>
    );
};
export default Fullpage;
