import React from 'react'
export const LayoutContainer = (props) => (
    <div>
        {props.children}
    </div>
)
export default LayoutContainer;