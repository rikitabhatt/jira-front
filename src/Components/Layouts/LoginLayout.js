import React from 'react'
import {  Alert } from '../../_component';
 const LoginLayout = ({ children }) => {
    return (
        <div className="page-wrapper">
            <Alert/>
            <div className="dashboard-wrapper simple-page-wrapper p-t-10">
                <div className="container">
                    <h3>Login</h3>
                     {children}
                </div>
            </div>
        </div>
    );
};
export default LoginLayout;
