import './App.css';
import Header from "./Components/Header";
import { Footer } from "./Components/Footer";
import Routes from './Components/Routes/Route';

import React, { useState, useEffect } from 'react';
import { BrowserRouter as Router} from "react-router-dom"

function App() {
  return (
      <Router>
        <Header/>
        <Routes/>
        <Footer/>
      </Router>
  );
}

export default App;
