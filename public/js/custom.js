/* *************** bootstrap date time picker *************** */
$(function () {
	$('#datetimepicker1').datetimepicker();
});

$(function() {
    $( "#start-sprint-date" ).datepicker({
        dateFormat: 'dd-mm-yy',
		 defaultDate:"31-05-2021"
    });
});
$(function() {
    $( "#end-sprint-date" ).datepicker({
        dateFormat: 'dd-mm-yy',
		 defaultDate:"04-06-2021"
    });
});

$(function() {
    $( "#date1" ).datepicker({
        dateFormat: 'dd-mm-yy',
		 defaultDate:"04-06-2021"
    });
});
/* *************** bootstrap tooltip *************** */
$(function () {
	$('[data-toggle="tooltip"]').tooltip()
  })
/* *************** custom multiple file upload *************** */
$(function(){
	$("#uploader").initUploader({
		selectOpts:{one:'jquery',two:'script',three:'net'},
		showDescription: true,
	});
});


/* *************** CK editor *************** */


/* *************** tabs *************** */
$('#pills-tab a').on('click', function (e) {
	e.preventDefault()
	$(this).tab('show')
  })

/* *************** sidebar menu *************** */

$(".sidebar-dropdown > a").click(function() {
    $(".sidebar-submenu").slideUp(200);
    if (
        $(this)
        .parent()
        .hasClass("active")
    ) {
        $(".sidebar-dropdown").removeClass("active");
        $(this)
            .parent()
            .removeClass("active");
    } else {
        $(".sidebar-dropdown").removeClass("active");
        $(this)
            .next(".sidebar-submenu")
            .slideDown(200);
        $(this)
            .parent()
            .addClass("active");
    }
});

$("#close-sidebar").click(function() {
    $(".page-wrapper").removeClass("toggled");
});
$("#show-sidebar").click(function() {
    $(".page-wrapper").addClass("toggled");
});


/* *************** scroll to top *************** */
		$(window).scroll(function(){
			if ($(window).scrollTop() >= 150) {
				$('.scroll-top').addClass('open');
				
			}
			else {
				$('.scroll-top').removeClass('open');
				
			}
});

/* *************** fixed header on scroll *************** */
			
				
		$(window).scroll(function(){
			if ($(window).scrollTop() >= 100) {
				$('.menu-wrapper').addClass('fixed-header');
				
			}
			else {
				$('.menu-wrapper').removeClass('fixed-header');
				
			}
		});
        
		
/* *************** Toggle menu *************** */	

		$('#toggle').click(function() {
		   $(this).toggleClass('active');
		   $('#overlay').toggleClass('open');
		  });
		  
	    $('.nav-link').click(function() {
			$(this).toggleClass('active');
		   $('#overlay').toggleClass('open');
		});
	
/* *************** Animation On Load *************** */	

	   new WOW().init();
	   
	   
/*********custom file upload *********/
$(document).ready(function () {
	bsCustomFileInput.init()
})



